from numpy import where
import numpy as np
from numpy.random import randint

from scipy.stats import norm, halfnorm, uniform
from scipy.signal import cheb2ord, cheby2, sosfilt


def biuniform(upper_min, upper_max, lower_min=None, lower_max=None, shape=None):
    if lower_max is None:
        lower_max = -upper_min
    if lower_min is None:
        lower_min = -upper_max  

    return where(
        randint(0, 2, shape) > 0,  # flip a fair coin
        uniform.rvs(upper_min, upper_max - upper_min, size=shape),
        uniform.rvs(lower_min, lower_max - lower_min, size=shape)
    )


def binorm(mu1, mu2=None, sigma=None, shape=None):
    if mu2 is None:
        mu2 = -mu1
    if sigma is None:
        sigma = abs(mu1 - mu2) / 3.14159
    return where(
        randint(0, 2, shape) > 0,  # flip a fair coin
        norm.rvs(mu1, sigma, size=shape),
        norm.rvs(mu2, sigma, size=shape)
    )


def bihalfnorm(upper_lim, lower_lim=None, sigma=None, shape=None):
    if lower_lim is None:
        lower_lim = -upper_lim
    if sigma is None:
        sigma = (upper_lim - lower_lim) / 3.14159

    vals = where(
        randint(0, 2, shape) > 0,    # flip a fair coin
        upper_lim - halfnorm.rvs(0, sigma, size=shape),
        lower_lim + halfnorm.rvs(0, sigma, size=shape)
    )

    vals[vals > upper_lim] = upper_lim - (vals[vals > upper_lim] - upper_lim)
    vals[vals < lower_lim] = lower_lim + (lower_lim - vals[vals < lower_lim])
    return vals


def gen_random_inputs(ranges, number, length, bias=None, mirrored=False, color='brown', amplitudes=None, oversampling=5):

    ranges = np.array(ranges)

    assert len(ranges.shape) == 2
    assert any([d == 2 for d in ranges.shape])
    if ranges.shape[0] == 2:
        ranges = ranges.T

    ranges.sort(axis=1)
    n_sigs = ranges.shape[0]

    bias = bias if bias is not None else np.mean(ranges, axis=1)

    if mirrored:
        ssl = int(np.floor(length / 2))
    else:
        ssl = length - 1

    signals = np.random.uniform(
        -1,
        1,
        (number, ssl, n_sigs),
    )

    signals = np.moveaxis(signals, 2, 1)
    signals = np.append(np.zeros((signals.shape[0], signals.shape[1], 1)), signals, axis=2)

    color_record = np.zeros(signals.shape[:2])

    def dobrown(signals):
        return np.cumsum(signals, axis=-1)

    if color == 'mixed':
        for test in range(signals.shape[0]):
            for sig in range(signals.shape[1]):
                if np.random.uniform(0, 1) > 0.5:
                    color_record[test, sig] = 1
                    signals[test, sig, :] = dobrown(signals[test, sig, :])

    elif color == 'brown':
        color_record = np.ones(signals.shape[:2])
        signals = dobrown(signals)

    n_classes = 1+int(color_record.max())

    if mirrored:
        signals = np.concatenate([signals, np.flip(signals, axis=-1)], axis=2)
    else:
        signals = signals

    # do filtering
    if oversampling >= 2:
        pass_edge = 1/oversampling
        stop_edge = pass_edge*1.2
        max_atten_pass = 1
        min_atten_stop = 80
        filt_order = cheb2ord(pass_edge, stop_edge, max_atten_pass, min_atten_stop)[0]
        filter = cheby2(filt_order, min_atten_stop, stop_edge, output='sos')

        signals = sosfilt(filter, signals)

    # adjust magnitudes
    for ch_ind, ch_sigs in enumerate(signals.swapaxes(0, 1)):
        for class_id in range(n_classes):
            if np.count_nonzero(color_record[:, ch_ind] == class_id):
                class_sigs = ch_sigs[color_record[:, ch_ind] == class_id, :]

                desired_neg_swing = ranges[ch_ind][0] - bias[ch_ind]
                desired_pos_swing = ranges[ch_ind][1] - bias[ch_ind]

                class_sigs[class_sigs > 0.0] = class_sigs[class_sigs > 0.0] * desired_pos_swing / class_sigs.max()
                class_sigs[class_sigs < 0.0] = class_sigs[class_sigs < 0.0] * desired_neg_swing / class_sigs.min()

                signals[color_record[:, ch_ind] == class_id, ch_ind, :] = class_sigs + bias[ch_ind]

                # maxes = np.max(class_sigs, axis=-1)
                # mins = np.min(class_sigs, axis=-1)
                # range_center = np.mean([np.max(maxes), np.min(mins)])
                # scales = np.ones(maxes.shape)
                #
                # minviolations = -(mins - ranges[:, 0])
                # maxviolations = maxes - ranges[:, 1]
                # minviolations = np.clip(minviolations, 0, None)
                # maxviolations = np.clip(maxviolations, 0, None)
                #
                # violations = maxviolations - minviolations
                #
                # for i in range(signals.shape[1]):
                #     scales[violations[:, i] > 0, i] = ranges[i, 1] / maxes[violations[:, i] > 0, i]
                #     scales[violations[:, i] < 0, i] = ranges[i, 0] / mins[violations[:, i] < 0, i]

    if amplitudes:
        if amplitudes == 'log':
            amplitudes = np.logspace(-2, 0, signals.shape[0] + 1, endpoint=True)[1:]
        elif amplitudes == 'linear':
            amplitudes = np.linspace(0.001, 1.0, signals.shape[0] + 1, endpoint=True)[1:]

        signals = (amplitudes * signals.T).T

    return signals.swapaxes(1,2)
