import inspect
import os
import time as time_module
import h5py
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from scipy.spatial import distance as sp_dist

import pyspectre
import waverunner
import xanity
from pyspectre.server import run_batch as ps_server_run_batch
from .prvs import gen_random_inputs


def parse_file_size(string_file_size):
    if isinstance(string_file_size, (int, float)):
        max_file_size = int(string_file_size)

    if isinstance(string_file_size, str):
        max_file_size = string_file_size.lower()

        if 'k' in max_file_size:
            max_file_size = int(max_file_size.split('m')[0]) * 2 ** 10

        elif 'm' in max_file_size:
            max_file_size = int(max_file_size.split('m')[0]) * 2 ** 20

        elif 'g' in max_file_size:
            max_file_size = int(max_file_size.split('m')[0]) * 2 ** 30

        elif max_file_size.isdigit():
            max_file_size = int(max_file_size)

    return max_file_size


def is_regular(time_vec):
    steps = np.diff(time_vec)
    return True if np.ptp(steps) < 1e-12 * np.median(steps) else False


def flatten_eps(block):
    return block.reshape(-1, block.shape[2])


def stack_eps(block, eplen):
    return block.reshape(-1, eplen, block.shape[1])


class BatchDatabase(object):

    def __init__(self,
                 data=None,
                 input_names=None,
                 signal_names=None,
                 backend='numpy',
                 file=None,
                 ):

        assert backend in ['numpy', 'np', 'xarray', 'xr', 'dask', 'dk'], "must use supported backend"

        if backend in ['numpy', 'np']:
            self.backend = 'np'
        elif backend in ['dask', 'dk']:
            self.backend = 'dk'
        elif backend in ['pandas', 'pd']:
            self.backend = 'pd'
        elif backend in ['xarray', 'xr']:
            self.backend = 'xr'
        else:
            raise NotImplementedError

        # if not os.path.isdir(os.path.split(filename)[0]):
        #     raise FileNotFoundError

        self.corrections = []

        if data is not None:
            self.data = None
            self.centers = None
            self.scales = None
            self.append_data(data)

        else:
            self.data = None

        self.signal_names = signal_names
        self.input_names = input_names
        self.output_names = [name for name in signal_names if name not in input_names]
        self.input_inds = [i for i, name in enumerate(signal_names) if name in input_names]
        self.output_inds = [i for i, name in enumerate(signal_names) if name not in input_names]

    # def __getattr__(self, item):
    #     if item not in dir(self):
    #         return getattr(self.data, item)

    def __repr__(self):
        return self.data.__repr__()

    def __getattr__(self, item):
        if self.data is not None and hasattr(self.data, item):
            return self.data.__getattribute__(item)
        else:
            raise AttributeError

    def __getitem__(self, index):
        return self.data[index]

    def indices(self, inds):
        return BatchDatabase(data=self.data[inds],
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def centered(self):
        return BatchDatabase(data=self.data - self.centers,
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def input(self):
        return BatchDatabase(data=self.data[:, :, self.input_inds],
                             signal_names=self.input_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def output(self):
        return BatchDatabase(data=self.data[:, :, self.output_inds],
                             signal_names=self.output_names,
                             input_names=self.input_names,
                             backend=self.backend)

    def copy(self, include_data=False):
        return BatchDatabase(
            data=self.data if include_data else None,
            signal_names=self.signal_names,
            input_names=self.input_names,
            backend=self.backend
        )

    @property
    def scaled(self):
        return BatchDatabase(data=self.data / self.scales,
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def standardized(self):
        return BatchDatabase(data=self.centered / self.scales,
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def transitions(self):
        origins = flatten_eps(self.data[:, :-1, :])
        destinations = flatten_eps(self.data[:, 1:, :])
        return BatchDatabase(data=np.stack([origins, destinations], axis=0),
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def corrected(self):
        if self.corrections:

            def corrected(data, fn):
                return lambda: fn(data)

            result = self.data

            for fn in self.corrections:
                if callable(fn):
                    result = corrected(result, fn)

            return BatchDatabase(data=result(),
                                 signal_names=self.signal_names,
                                 input_names=self.input_names,
                                 backend=self.backend)
        else:
            raise ValueError('no correction functions defined')

    def get_recent(self, quantity):
        return BatchDatabase(data=self.data[-quantity:],
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    def get_samples(self, quantity):
        inds = np.random.choice(np.arange(self.data.shape[0]), quantity)
        return BatchDatabase(data=self.data[inds],
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    def dump(self, file=None):
        if file:
            self.save_data(file)

        self.data = None
        self.centers = None
        self.scales = None

    def to_states(self, memory=0, include_present=True):
        raise NotImplemented
        # go through and check
        state_obs = []
        start_ind = 0 if include_present else 1
        for i in range(start_ind, memory + 1):
            state_obs.append(
                np.concatenate([
                    np.tile(np.expand_dims(self.data[:, 0, :], 2), (1, i, 1)),
                    self.data
                ],
                    axis=2)[:, :, :self.data.shape[2]]
            )

        x = np.concatenate(state_obs, axis=1) if state_obs else np.empty((self.data.shape[0], 0, self.data.shape[2]))

        return BatchDatabase(data=x,
                             signal_names=self.signal_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def flat_signals(self):
        return flatten_eps(self.data)

    def flat_inds2obs_inds(self, inds):
        return np.unravel_index(inds, dims=(self.data.shape[0], self.data.shape[1]))

    def from_obs_time_inds(self, bool_array):
        assert len(self.data.shape) == 3
        raise NotImplemented
        # don't remember what this is.... must check it
        result = np.empty((0, self.data.shape[1]))
        while np.count_nonzero(bool_array):
            result = np.concatenate([result, self.data.swapaxes(1, 2)[bool_array > 0]], axis=0)
            bool_array = bool_array - 1
            bool_array[bool_array < 0] = 0

        return result

    def append_data(self, data, compute=None):
        if self.backend == 'np':

            assert len(data.shape) == 3

            if compute is True:
                do_compute = True
            elif compute is False:
                do_compute = False
            elif compute is None:
                if hasattr(self, 'compute') and self.compute is True:
                    do_compute = True
                else:
                    do_compute = False

            if do_compute:
                data = self.compute_data(data)

            if self.data is None:
                self.data = data
            else:
                self.data = np.concatenate([self.data, data], axis=0)

            self.centers = np.mean(self.data, axis=(0, 1))
            self.scales = np.std(self.data, axis=(0, 1))

        return self

    def save_data(self, file=None):

        if file is None:
            file = 'batchdata-{}.hd5'.format(xanity.run_id)

        root, file = os.path.split(file)
        parts = file.split('.')
        base, ext = '.'.join(parts[:-1]), parts[-1]
        file = os.path.join(root, base + '.hd5')

        with h5py.File(file) as f:
            f.create_dataset('data', data=self.data, compression="gzip")
            f.create_dataset('scales', data=self.scales)
            f.create_dataset('centers', data=self.centers)

    def load_data(self, file_list=None, fraction=1, shuffle=True):

        if file_list is None:
            if self.backend == 'np':
                file_list = [os.path.join(os.getcwd(), f) for f in os.listdir() if f.endswith('.hd5')]

        elif isinstance(file_list, str):
            file_list = [file_list]

        for file in file_list:
            if self.backend == 'np':
                with h5py.File(file) as f:
                    data_size = f['data'].shape[0]
                    if 0 < fraction < 1:
                        load_ct = int(fraction * data_size)
                    else:
                        load_ct = data_size

                    if shuffle:
                        inds = np.random.choice(data_size, load_ct, replace=False)
                        inds.sort()
                    else:
                        inds = np.array(list(range(load_ct)))

                    self.append_data(f['data'][inds, :, :], compute=False)

        return True


#        if file_list is None:
#
#            cv_datapath = xanity.subdir('cv_data')
#            cv_files = list(os.listdir(cv_datapath))
#
#            raw_datapath = xanity.subdir('raw_data')
#            raw_files = list(os.listdir(raw_datapath))
#
#            need_processing = [item for item in raw_files if item not in cv_files]
#            for item in need_processing:
#                s = os.path.join(raw_datapath, item)
#                d = os.path.join(cv_datapath, item)
#
#                da = xr.open_dataarray(s)
#                pd = modeling.PairedBatchData(dataarray1=da, input_names=self.input_signals,
#                                              pairs=self.observation_pairs)
#                pd.to_netcdf(d)
#
#            file_list = [os.path.join(cv_datapath, file) for file in list(os.listdir(cv_datapath))]
#
#        data = None
#        for file in file_list:
#            d = xr.open_dataset(file, chunks={'id': -1, 'name': -1, 'time': -1}).to_array().squeeze(drop=True)
#            if data is None:
#                data = d
#            else:
#                data = xr.concat([data, d], dim='id')
#
#        # data = xr.concat([
#        #     item.dropna('id') for _, item in data.groupby('variable')
#        # ], dim='id')
#
#        data = modeling.PairedBatchData(data)
#
#        return data


class PairedBatchDatabase(BatchDatabase):

    def __init__(self,
                 data=None,
                 input_names=None,
                 signal_names=None,
                 pairs=None,
                 data2=None,
                 backend='numpy',
                 file=None,
                 compute=True,
                 ):

        assert backend in ['numpy', 'np', 'xarray', 'xr', 'dask', 'dk']

        pairs = np.array(pairs)
        pairs = pairs if pairs.shape[0] == 2 else pairs.T
        self.pairs = pairs

        self.compute = compute

        self.input_names = list(input_names) if input_names \
            else [s for s in signal_names if s not in np.array(self.pairs).flatten()]

        if compute:
            self._incoming_signal_names = list(signal_names)
            self.cm_names = ['+'.join(pp) for pp in self.pairs.T]
            self.dm_names = ['-'.join(pp) for pp in self.pairs.T]

        else:
            self.cm_names = [name for name in signal_names if '+' in name]
            self.dm_names = [name for name in signal_names if '-' in name]

        # these refer to arrangement of the output of self.compute(data):
        self.input_inds = [i for i, name in enumerate(self.input_names)]
        self.cm_inds = [self.input_inds[-1] + 1 + i for i, name in enumerate(self.cm_names)]
        self.dm_inds = [self.cm_inds[-1] + 1 + i for i, name in enumerate(self.dm_names)]
        self.error_ind = self.dm_inds[-1] + 1

        self.signal_names = self.input_names + self.cm_names + self.dm_names + ['error_metric']

        if data is not None and data2 is None:

            super(PairedBatchDatabase, self).__init__(
                data=data,
                signal_names=self.signal_names,
                input_names=self.input_names,
                backend=backend,
                file=file,
            )

        elif data is None and data2 is None:

            super(PairedBatchDatabase, self).__init__(
                data=None,
                signal_names=self.signal_names,
                input_names=self.input_names,
                backend=backend,
                file=file,
            )

        # elif backend == 'xarray':
        #     self._backend = 'xr'
        #
        #     self._wrapped_xarray_data_ = None
        #     self._recursing = False
        #
        #     assert isinstance(data, (xr.DataArray, xr.Dataset))
        #     if data2:
        #         assert isinstance(data2, (xr.DataArray, xr.Dataset))
        #
        #     avail_in1 = list(data.coords['name'].data)
        #
        #     if not data2 \
        #             and (any([name.endswith('_dm') for name in avail_in1])
        #                  and any([name.endswith('_cm') for name in avail_in1])):
        #         self.input_names = [name for name in avail_in1 if
        #                             (not name.endswith('_dm') and not name.endswith('_cm'))]
        #         data = data
        #
        #     else:
        #         if not data2:
        #             need_from1 = list(np.concatenate([np.array(input_names).flatten(), np.array(pairs).flatten()]))
        #             if not all([name in avail_in1 for name in need_from1]):
        #                 print("couldn't find everything in array1, and array2 not provided. limping...")
        #             in_from1 = [name for name in input_names if name in avail_in1]
        #             in_from2 = []
        #             state_from1 = [name for name in np.array(pairs).flatten() if name in avail_in1]
        #             state_from2 = []
        #
        #             data = data.sel(name=in_from1)
        #
        #         else:
        #             in_from1 = [name for name in input_names if name in avail_in1]
        #             need_from2 = [iin for iin in input_names if iin not in avail_in1]
        #             avail_in2 = data2.coords['name']
        #             in_from2 = [name for name in need_from2 if name in avail_in2]
        #
        #             if not all([name in avail_in2 for name in need_from2]):
        #                 print("couldn't get complete input information. limping along...")
        #
        #             data = data.sel(name=in_from1)
        #             data = xr.concat([data, data2.sel(name=in_from2)], dim='name')
        #
        #         self.input_names = list(data.coords['name'].values)
        #
        #         keep_pairs = []
        #         if not data2:
        #             for pair in pairs:
        #                 if pair[0] in avail_in1 and pair[1] in avail_in1:
        #                     keep_pairs.append(pair)
        #         else:
        #             for pair in pairs:
        #                 if pair[0] in avail_in1 and pair[1] in avail_in2:
        #                     keep_pairs.append(pair)
        #
        #         if len(keep_pairs) != len(pairs):
        #             print("couldn't get complete list of pairs. sure that order is correct?")
        #
        #         self.pairs = tuple(keep_pairs)
        #
        #         if not data2:
        #             if hasattr(data, 'data'):
        #                 a = data.sel(name=[p[1] for p in self.pairs]).data
        #                 b = data.sel(name=[p[0] for p in self.pairs]).data
        #             elif hasattr(data, 'values'):
        #                 a = data.sel(name=[p[1] for p in self.pairs]).values()
        #                 b = data.sel(name=[p[0] for p in self.pairs]).values()
        #
        #             cm = xr.DataArray(
        #                 data=(a + b) / 2.0,
        #                 dims=['id', 'name', 'time'],
        #                 coords={'id': data.id,
        #                         'name': [','.join(p) + '_cm' for p in self.pairs],
        #                         'time': data.time})
        #
        #             dm = xr.DataArray(
        #                 data=(a - b) / 2.0,
        #                 dims=['id', 'name', 'time'],
        #                 coords={'id': data.id,
        #                         'name': [','.join(p) + '_dm' for p in self.pairs],
        #                         'time': data.time})
        #
        #         else:
        #             cm = xr.DataArray(
        #                 data=(data.sel(name=[p[1] for p in self.pairs]).data
        #                       + data.sel(name=[p[0] for p in self.pairs]).data) / 2.0,
        #                 dims=['id', 'name', 'time'],
        #                 coords={'id': data.id,
        #                         'name': [','.join(p) + '_cm' for p in self.pairs],
        #                         'time': data.time, })
        #
        #             dm = xr.DataArray(
        #                 data=(data.sel(name=[p[1] for p in self.pairs]).data
        #                       - data.sel(name=[p[0] for p in self.pairs]).data) / 2.0,
        #                 dims=['id', 'name', 'time'],
        #                 coords={'id': data.id,
        #                         'name': [','.join(p) + '_dm' for p in self.pairs],
        #                         'time': data.time, })
        #
        #         # mid = Mi.from_tuples(
        #         #     [('input', name) for name in self.input_names]
        #         #     + [('cm', name) for name in data.coords['name'].values if name.endswith('_cm')]
        #         #     + [('dm', name) for name in data.coords['name'].values if name.endswith('_dm')]
        #         # )
        #         # data.coords['name'] = mid
        #
        #         data = xr.concat([data, cm, dm], dim='name')
        #
        #     self.cm_names = [name for name in data.coords['name'].values if name.endswith('_cm')]
        #     self.cm_inds = [data.get_index('name').get_loc(name) for name in self.cm_names]
        #     self.dm_names = [name for name in data.coords['name'].values if name.endswith('_dm')]
        #     self.dm_inds = [data.get_index('name').get_loc(name) for name in self.dm_names]
        #
        #     self.signal_names = self.cm_names + self.dm_names
        #
        #     # centers = data.data.transpose((0, 2, 1,)).reshape((-1, data.shape[1])).mean(axis=0)
        #     # scales = data.data.transpose((0, 2, 1,)).reshape((-1, data.shape[1])).mean(axis=0)
        #     #
        #     # centers = centers.compute() if hasattr(centers, 'compute') else centers
        #     # scales = scales.compute() if hasattr(scales, 'compute') else scales
        #     #
        #     # self.centers = xr.DataArray(centers, dims=['name'], coords={'name': data.coords['name']})
        #     # self.scales = xr.DataArray(scales, dims=['name'], coords={'name': data.coords['name']})
        #
        #     self.centers = data.groupby('name').mean(dim=xr.ALL_DIMS)
        #     self.scales = data.groupby('name').std(dim=xr.ALL_DIMS)
        #
        #     if 'error_metric_em' not in data.coords['name']:
        #         em_scales = self.scales.sel(name=self.dm_names)
        #         em_centers = self.centers.sel(name=self.dm_names)
        #         null = np.expand_dims(np.zeros(em_scales.shape), axis=0)
        #
        #         def score(x, axis=None):
        #             return sp_dist.cityblock(x, null)
        #             # return cdist(x, null, metric='cityblock')
        #
        #         locs = (data.sel(name=self.dm_names).stack(z=('id', 'time')) - self.centers) / self.scales
        #
        #         em = locs.transpose().groupby('z').reduce(score)
        #         em = em.unstack('z').rename(z_level_0='id', z_level_1='time')
        #         em = em.assign_coords(name='error_metric_em')
        #
        #         data = xr.concat([data, em], dim='name')
        #
        #     self._wrapped_xarray_data_ = data

    # @property
    # def state_names(self):
    #     if self._format == 'np':
    #         return self._state_names
    #     elif self._format == 'xarray':
    #         return self._state_names

    # @property
    # def cm_scale(self):
    #     return self.data.scales[list(self.cm_inds)]
    #
    # @property
    # def cm_center(self):
    #     return self.data.centers[list(self.cm_inds)]
    #
    # @property
    # def cm_standard(self):
    #     return (self.data[:, list(self.cm_inds), :] - self.cm_center ) / self.cm_scale

    # @property
    # def dm_scale(self):
    #     return self.data.scales[list(self.dm_inds)]
    #
    # @property
    # def dm_center(self):
    #     return self.data.centers[list(self.dm_inds)]
    #
    # @property
    # def dm_standard(self):
    #     return (self.data[:, list(self.dm_inds), :] - self.dm_center) / self.dm_scale

    def copy(self, include_data=False):
        db = PairedBatchDatabase(
            data=self.data if include_data else None,
            pairs=self.pairs,
            signal_names=self._incoming_signal_names,
            input_names=self.input_names,
            backend=self.backend,
            compute=self.compute)
        return db

    def compute_error_vec(self, cm_data, dm_data):
        o_shape = dm_data.shape
        dm_data = flatten_eps(dm_data)
        scale = flatten_eps(cm_data).std(axis=0)
        rel_scale = scale/scale.max()
        dm_data = np.divide(dm_data, rel_scale, where=scale != 0)
        null = np.zeros((1, dm_data.shape[1]))

        error = sp_dist.cdist(null, dm_data, metric='cityblock').reshape((o_shape[0], o_shape[1], 1))
        return error

    def indices(self, inds):
        return PairedBatchDatabase(data=self.data[inds],
                                   pairs=self.pairs,
                                   signal_names=self.signal_names,
                                   input_names=self.input_names,
                                   backend=self.backend,
                                   compute=False)

    @property
    def in_cm(self):
        return BatchDatabase(data=self.data[:,:, self.input_inds + self.cm_inds],
                             signal_names=self.input_names + self.cm_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def in_dm(self):
        return BatchDatabase(data=self.data[:, :, self.input_inds + self.dm_inds],
                             signal_names=self.input_names + self.dm_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def cm(self):
        return BatchDatabase(data=self.data[:, :, self.cm_inds],
                             signal_names=self.cm_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def dm(self):
        return BatchDatabase(data=self.data[:, :, self.dm_inds],
                             signal_names=self.dm_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def cm_dm(self):
        return BatchDatabase(data=self.data[:, :, self.cm_inds + self.dm_inds],
                             signal_names=self.cm_names + self.dm_names,
                             input_names=self.input_names,
                             backend=self.backend)

    @property
    def unpaired(self):
        return BatchDatabase(
            data=np.concatenate([
                self.data[:, :, self.input_inds],
                self.data[:, :, self.cm_inds] + self.data[:, :, self.dm_inds],
                self.data[:, :, self.cm_inds] - self.data[:, :, self.dm_inds],
                ],
                axis=2),
            signal_names=self.input_names + list(np.array([name.split('-') for name in self.dm_names]).T.flatten()),
            input_names=self.input_names,
            backend=self.backend)

    @property
    def error(self):
        return BatchDatabase(data=self.data[:, : , self.error_ind],
                             signal_names='error_metric',
                             input_names=self.input_names,
                             backend=self.backend)

    def compute_data(self, data):
        # if hasattr(self, 'database'):
        #     if not data.shape[1] == self.data.shape[1]-1 and data.shape[2] == self.data.shape[2]:
        #         raise ValueError
        if not self.compute:
            return data

        assert self.pairs.shape[0] == 2

        inpt_inds = [self._incoming_signal_names.index(item) for item in self.input_names]
        ckt1_inds = [self._incoming_signal_names.index(item) for item in self.pairs[0]]
        ckt2_inds = [self._incoming_signal_names.index(item) for item in self.pairs[1]]

        inpt_data = data[:, :, inpt_inds]
        cm_data = np.mean([data[:, :, ckt1_inds],  data[:, :, ckt2_inds]], axis=0)
        dm_data = np.mean([data[:, :, ckt1_inds], -data[:, :, ckt2_inds]], axis=0)
        error = self.compute_error_vec(cm_data, dm_data)

        return np.concatenate([inpt_data, cm_data, dm_data, error], axis=2)

    def save_data(self, file=None):

        if file is None:
            file = 'pairedbatchdata-{}.hd5'.format(xanity.run_id)

        root, file = os.path.split(file)
        parts = file.split('.')
        base, ext = '.'.join(parts[:-1]), parts[-1]
        file = os.path.join(root, base + '.hd5')

        super(PairedBatchDatabase, self).save_data(file)

        with h5py.File(file) as f:
            f.create_dataset('input_inds', data=self.input_inds)
            f.create_dataset('cm_inds', data=self.cm_inds)
            f.create_dataset('dm_inds', data=self.dm_inds)
            f.create_dataset('error_ind', data=self.error_ind)

    def load_data(self, file_list=None, fraction=1, shuffle=True):

        if file_list is None:
            if self.backend == 'np':
                file_list = [os.path.join(os.getcwd(), f) for f in os.listdir() if f.endswith('.hd5')]

        elif isinstance(file_list, str):
            file_list = [file_list]

        for file in file_list:
            assert super(PairedBatchDatabase, self).load_data(file, fraction, shuffle)

            if self.backend == 'np':
                with h5py.File(file) as f:
                    self.input_inds = f['input_inds'][:]
                    self.cm_inds = f['cm_inds'][:]
                    self.dm_inds = f['dm_inds'][:]
                    self.error_ind = f['error_ind'][()]

        return True

    #        if file_list is None:
    #
    #            cv_datapath = xanity.subdir('cv_data')
    #            cv_files = list(os.listdir(cv_datapath))
    #
    #            raw_datapath = xanity.subdir('raw_data')
    #            raw_files = list(os.listdir(raw_datapath))
    #
    #            need_processing = [item for item in raw_files if item not in cv_files]
    #            for item in need_processing:
    #                s = os.path.join(raw_datapath, item)
    #                d = os.path.join(cv_datapath, item)
    #
    #                da = xr.open_dataarray(s)
    #                pd = modeling.PairedBatchData(dataarray1=da, input_names=self.input_signals,
    #                                              pairs=self.observation_pairs)
    #                pd.to_netcdf(d)
    #
    #            file_list = [os.path.join(cv_datapath, file) for file in list(os.listdir(cv_datapath))]
    #
    #        data = None
    #        for file in file_list:
    #            d = xr.open_dataset(file, chunks={'id': -1, 'name': -1, 'time': -1}).to_array().squeeze(drop=True)
    #            if data is None:
    #                data = d
    #            else:
    #                data = xr.concat([data, d], dim='id')
    #
    #        # data = xr.concat([
    #        #     item.dropna('id') for _, item in data.groupby('variable')
    #        # ], dim='id')
    #
    #        data = modeling.PairedBatchData(data)
    #
    #        return data

    def plot(self, index=None):

        if index is not None:
            if isinstance(index, str):
                inds = int(index[0])
                if len(index) > 1:
                    sig = int(index[1])
            else:
                if isinstance(index, int):
                    index = str(index)
                    inds = int(index[0])
                    if len(index) > 1:
                        sig = int(index[1])

        else:
            n_tests = int(self.sizes['id'])
            inds = list(range(0, n_tests, int(np.floor(n_tests / 20))))

            n_sigs = len(self.dm_names)
            sig = list(range(0, n_sigs, int(np.floor(n_sigs / 3))))

        fig = plt.figure()
        ax0 = fig.add_subplot(111)

        # Turn off axis lines and ticks of the big subplot
        ax0.spines['top'].set_color('none')
        ax0.spines['bottom'].set_color('none')
        ax0.spines['left'].set_color('none')
        ax0.spines['right'].set_color('none')
        ax0.tick_params(labelcolor='w', top=False, bottom=False, left=False, right=False)

        axes = fig.subplots(nrows=3, ncols=1, sharex=True)
        fig.tight_layout()

        for ind in inds:
            axes[0].plot(
                self['time'],
                self.isel(id=ind).sel(name=self.input_names).squeeze().T)
            axes[0].set_title('inputs')

            axes[1].plot(
                self['time'],
                self.isel(id=ind).sel(name=np.array(self.cm_names)[sig]).squeeze().T)
            axes[1].set_title('common mode')

            axes[2].plot(
                self['time'],
                self.isel(id=ind).sel(name=np.array(self.dm_names)[sig]).squeeze().T)
            axes[2].set_title('differential mode')

        ax0.set_xlabel('Time (s)')
        ax0.set_ylabel('Signal Amplitude (v/A)')
        plt.ticklabel_format(axis='both', style='sci', useOffset=True)

    # def save(self, filename):
    #     # path, file = os.path.split(filename)
    #     # index_file = os.path.join(path, '.' + file + '.multiindex.pkl')
    #     # with open(index_file, 'wb') as f:
    #     #     pickle.dump(data.coords['name'].values, f)
    #     # data.reset_index('name').to_netcdf(filename)
    #
    #     data.to_netcdf(filename)
    #
    # def load(self, filename):
    #     # path, file = os.path.split(filename)
    #     # index_file = os.path.join(path, '.' + file + '.multiindex.pkl')
    #     # with open(index_file, 'rb') as f:
    #     #     index = pickle.load(f)
    #     #
    #     # data = xr.open_dataarray(filename)
    #     # data.coords['name'] = Mi.from_tuples(index)
    #
    #     data = xr.open_dataarray(filename)


# class ExperimentalData(object):
#     def __init__(self, statedims=None, actiondims=None, responsedims=None, max_records=500):
#         self.states = pd.DataFrame(columns=[None] * statedims)
#         self.actions = pd.DataFrame(columns=[None] * actiondims)
#         self.responses = pd.DataFrame(columns=[None] * responsedims)
#         self.counter = 0
#         self.dump_ind = 0
#         self.max_records = max_records
#
#     def log(self, states, actions, responses):
#
#         nnrecords = states.shape[0]
#
#         assert nnrecords == actions.shape[0] and nnrecords == responses.shape[0]
#
#         if self.counter + nnrecords > self.max_records:
#             available = self.max_records - self.counter
#             self.log(states[:available], actions[:available], responses[:available])
#             self.flush()
#             self.log(states[available:], actions[available:], responses[available:])
#             return
#
#         else:
#             self.states.append(states)
#             self.actions.append(actions)
#             self.responses.append(responses)
#
#     def flush(self):
#         name = 'exp_log_'
#         self.states.to_hdf(name + 'states_' + str(self.dump_ind))
#         self.actions.to_hdf(name + 'actions_' + str(self.dump_ind))
#         self.responses.to_hdf(name + 'responses_' + str(self.dump_ind))
#
#         self.states = pd.DataFrame(columns=[None] * self.states.shape[1])
#         self.actions = pd.DataFrame(columns=[None] * self.actions.shape[1])
#         self.responses = pd.DataFrame(columns=[None] * self.responses.shape[1])


class Vectorized(object):
    def __init__(self):
        # if parameters:
        #     assert isinstance(parameters,list) and all([isinstance(p,str) for p in parameters])
        #     self._param_dict = {name: None for name in parameters}
        # elif param_dict:
        #     assert isinstance(param_dict, dict)
        #     self._param_dict = param_dict

        self._vectorized_basemodel = self
        self._vectorized_baseclass = self.__class__ if self.__class__ is not Vectorized else object

        if hasattr(inspect, 'getfullargspec'):
            self._vectorized_param_names = tuple(
                name for name in inspect.getfullargspec(self.__init__)[0] if name != 'self')
        else:
            self._vectorized_param_names = tuple(
                name for name in inspect.getargspec(self.__init__)[0] if name != 'self')

        self._vectorized_param_shapes = tuple(
            np.array([getattr(self, name)]).shape for name in self._vectorized_param_names)
        self._vectorized_param_sizes = tuple(
            np.array(getattr(self, name)).size for name in self._vectorized_param_names)

        # for item in dir(self._basemodel):
        #     if item not in self.frozen_attrs:
        #         setattr(self, item, getattr(self._basemodel, item))

    def __dict__(self):
        return dict(self.items())

    def values(self):
        return tuple(getattr(self._vectorized_basemodel, p) for p in self._vectorized_param_names)

    def keys(self):
        return self._vectorized_param_names

    def items(self):
        return zip(self.keys(), self.values())

    def vectorized(self):
        return np.hstack([np.array(item).flatten() for item in self.values()]).flatten()

    def __getattr__(self, item):
        local_meths = ['items', 'keys', 'values', '__dict__', 'vectorized', 'from_vector']
        if item not in local_meths and not item.startswith('_vectorized'):
            return getattr(self._vectorized_basemodel, item)

    @staticmethod
    def from_vector(classname, vector):
        exaxmple = classname()
        split_inds = np.cumsum(exaxmple._vectorized_param_sizes)[:-1]
        params = np.array_split(vector, split_inds)
        for i, p in enumerate(params):
            params[i] = p.reshape(exaxmple._vectorized_param_shapes[i])
        kwargs = dict(zip(exaxmple._vectorized_param_names, params))
        return classname(**kwargs)


class Block(Vectorized):
    def __init__(self, input_dim=1, output_dim=1, requires_regular_samples=False, input_range=None,
                 input_bias=None, highpass_nodes=None, dtype='float64'):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.input_range = (-np.Inf * np.ones((input_dim,)), np.Inf * np.ones((input_dim,))) \
            if input_range is None else input_range
        self._req_regular_samples = requires_regular_samples
        self._sim = self.sim
        self.database = None
        self.observation_pairs = None
        self.highpass_nodes = highpass_nodes
        self.input_bias = input_bias
        self.dtype = dtype
        setattr(self, 'sim', self.wrapped_sim)
        super(Block, self).__init__()

    def __getattr__(self, item):
        if item == 'sim':
            return lambda sigs, time: self.wrapped_sim(sigs, time)
        elif item == 'data':
            return self.database.data
        else:
            return None

    def run(self, inputs, time):
        outputs = []
        t = time.flatten()
        for input in inputs:
            outputs.append(self.wrapped_sim(input, t))
        return np.stack(outputs), time

    def new_database(self):
        return self.database.copy(include_data=False)

    def wrapped_sim(self, signals, time=None):
        if len(signals.shape) == 1:
            assert self.input_dim == 1

        elif len(signals.shape) == 2:
            assert self.input_dim == signals.shape[0]

        elif len(signals.shape) == 3:
            assert self.input_dim == signals.shape[1]

        if time is not None and self._req_regular_samples and not is_regular(time):
            # resample input
            ts = np.min(np.diff(time))
            new_time = np.arange(time[0], time[-1], ts)
            signals = interp1d(time, signals, 'cubic')(new_time)
            response = self._sim(signals, new_time)
            response = np.interp(time, new_time, response)
        else:
            response = self._sim(signals, time)

        # if self.output_dim == 1:
        #     assert response.shape[0] == 1 or len(response.shape) == 1
        # else:
        #     assert response.shape[0] == self.output_dim
        return response

    def requires_regular_samples(self):
        self._req_regular_samples = True

    def gen_random_inputs(self, *args, **kwargs):
        if not 'input_bias' in kwargs and self.input_bias:
            input_bias = self.input_bias
        return gen_random_inputs(self.input_range, *args, bias=input_bias, **kwargs)

    def append_data(self,
                    data=None,
                    inputs=None,
                    signal_order=None,
                    pairs=None,
                    data2=None,
                    backend='numpy',
                    file=None,
                    ):

        if self.database is None:
            if pairs is not None or self.observation_pairs is not None:
                self.database = PairedBatchDatabase(
                    data=data,
                    signal_names=signal_order if signal_order else self.signal_names,
                    input_names=inputs,
                    pairs=pairs if pairs else self.observation_pairs,
                    data2=data2,
                    backend=backend,
                    file=file)
            else:
                self.database = BatchDatabase(
                    data=None,
                    signal_names=None,
                    input_names=None,
                    backend='numpy',
                    file=None,
                )
        else:
            self.database.append_data(data)

    def save_data(self, file=None):
        if self.database is None:
            print('there is no data to save')
            return
        else:
            self.database.save_data(file)

    def load_data(self, file=None, fraction=1, shuffle=True):
        self.database.load_data(file, fraction, shuffle)


class SpiceCircuit(Block):
    def __init__(self, netlist, sim_directory, pwl_inputs=None, input_range=None,
                 input_signal_names=None, observations=None,
                 time_vector=None, max_job_size=100, n_workers=16,
                 max_file_size='100M', signal_transformations={}, timeout=240,
                 parallel=False, verbosity=1, local=True, logging=False, asynch_repl='thread',
                 **kwargs):

        if isinstance(pwl_inputs, str):
            pwl_inputs = [pwl_inputs]

        if isinstance(observations, str):
            observations = [observations]

        if isinstance(input_signal_names, str):
            input_signal_names = [input_signal_names]

        if signal_transformations:
            raise NotImplementedError
        #     assert isinstance(signal_transformations, dict)
        #     assert all([s in observations for s in signal_transformations.keys()]), "every transformation" \
        #                                                                             " should also be an observation"
        #     signals_needed_for_trans = [tokenize.tokenize(v) for v in signal_transformations.values()]
        # self.signals_for_trans = signals_needed_for_trans if signal_transformations is not None else None
        # self.signal_transformations = signal_transformations

        if time_vector is not None:
            assert sum([1 if d > 1 else 0 for d in time_vector.shape]) == 1
            time_vector = time_vector.squeeze()

        self.time_vector = time_vector
        self.input_signal_names = input_signal_names
        self.observation_names = observations

        self.max_job_size = int(max_job_size)
        self.expected_workers = int(n_workers)
        self.timeout = int(timeout)
        self.max_file_size = parse_file_size(max_file_size) if max_file_size is not None else 100 * 2 ** 20
        self.external_worker = False
        self.parallel = parallel
        self.verbosity = verbosity
        self.local = local
        self.logging = logging
        self.asynch_repl = asynch_repl

        # assign/create pwlinputs
        self.pwl_inputs = {}
        if isinstance(pwl_inputs, dict):
            for key, val in pwl_inputs.items():
                if isinstance(val, pyspectre.PwlInput):
                    self.pwl_inputs.update({key: val})
                elif isinstance(val, str):
                    self.pwl_inputs.update({
                        key:
                            pyspectre.PwlInput(val, max_n_points=time_vector.size if time_vector is not None else 100)
                    })
                else:
                    raise NotImplementedError

        elif isinstance(pwl_inputs, (list, tuple)):
            for inpt in pwl_inputs:
                if type(inpt) is pyspectre.PwlInput:
                    self.pwl_inputs.update({inpt.name, inpt})
                elif type(inpt) is str:
                    self.pwl_inputs.update({inpt:
                                                pyspectre.PwlInput(inpt,
                                                                   max_n_points=time_vector.size if time_vector is not None else 100)
                                            })
                else:
                    raise NotImplementedError

        self.all_signals = list(self.input_signal_names) + list(self.observation_names)
        self.database = BatchDatabase(data=None, signal_names=self.all_signals, input_names=self.input_signal_names)

        super(SpiceCircuit, self).__init__(input_dim=len(pwl_inputs), input_range=input_range,
                                           requires_regular_samples=False, output_dim=len(self.all_signals),
                                           **kwargs)

        self.circuit = pyspectre.Circuit(
            netlist=netlist,
            simdirectory=sim_directory,
            pwl_inputs=list(self.pwl_inputs.values()),
        )

        self.circuit.pack_up()

    def sim_batch(self, voltss, times=None, parallel=None, verbosity=None, local=None, resample=None, logging=None, asynch_repl='',
                  dtype=None, **kwargs):

        if parallel is None:
            if hasattr(self, 'parallel') and self.parallel is not None:
                parallel = self.parallel
            else:
                parallel = False

        if verbosity is None:
            if hasattr(self, 'verbosity') and self.verbosity is not None:
                verbosity = self.verbosity
            else:
                verbosity = 1

        if times is None:
            if hasattr(self, 'time_vector') and self.time_vector is not None:
                assert self.time_vector.size == voltss.shape[-1]
                times = self.time_vector
            else:
                raise ValueError('bad time vector')

        if local is None:
            if hasattr(self, 'local') and self.local is not None:
                local = self.local
            else:
                local = True

        if resample is None:
            if hasattr(self, 'resample') and self.resample is not None:
                resample = self.resample
            else:
                resample = True

        if logging is None:
            if hasattr(self, 'logging') and self.logging is not None:
                logging = self.logging
            else:
                logging = False

        if asynch_repl == '':
            if hasattr(self, 'asynch_repl') and self.asynch_repl != '':
                asynch_repl = self.asynch_repl
            else:
                asynch_repl = 'thread'

        if dtype is None:
            if hasattr(self, 'dtype') and self.dtype is not None:
                dtype = self.dtype
            else:
                dtype = 'float64'

        if times.shape[:2] != voltss.shape[:2]:
            if len(times.shape) == 1:
                times = np.expand_dims(times, 0)
            if len(times.shape) == 3:
                assert times.shape[2] == 1
                times = times.reshape(1, -1)
            if len(times.shape) > 2:
                raise ValueError('time dimension is incorrect')
            if times.shape[0] == 1:
                times = np.broadcast_to(times, (voltss.shape[0], times.shape[1]))
            if times.shape == voltss.shape[::-1]:
                times = times.T

        times, signals = self.circuit.run_batch(
            voltss, times,
            signal_names=self.all_signals, resample=resample,
            parallel=parallel, n_workers=self.expected_workers,
            verbosity=verbosity,
            max_jobsize=self.max_job_size,
            local=local,
            logging=logging,
            asynch_repl=asynch_repl,
            dtype=dtype,
            **kwargs
        )

        return times, signals


class SpiceCircuitPair(SpiceCircuit):
    def __init__(self, netlist, sim_directory, observation_pairs,
                 pwl_inputs=None, input_range=None,
                 input_signal_names=None, observations=None,
                 time_vector=None,
                 **kwargs):

        assert isinstance(observation_pairs, (tuple, list)), "must provide iterable of tuples indicating associations"

        if observations is None:
            observations = np.array(observation_pairs).flatten().squeeze()
        else:
            observations = list(set(list(observations) + list(np.array(observation_pairs).flatten().squeeze())))

        super(SpiceCircuitPair, self).__init__(netlist=netlist, sim_directory=sim_directory, pwl_inputs=pwl_inputs,
                                               input_range=input_range,
                                               input_signal_names=input_signal_names, observations=observations,
                                               time_vector=time_vector,
                                               **kwargs)

        observation_pairs = np.array(observation_pairs)
        self.observation_pairs = observation_pairs if observation_pairs.shape[1] == 2 else observation_pairs.T
        self.database = PairedBatchDatabase(data=None, pairs=self.observation_pairs,
                                            input_names=self.input_signal_names, signal_names=self.all_signals)


class AugmentedBlock(Block):
    def __init__(self, base_block):
        self._augmentors = []
        self._base_block = base_block
        super(AugmentedBlock, self).__init__(
            input_dim=self.base_block.input_dim,
            output_dim=self.base_block.input_dim,
            requires_regular_samples=self.base_block.requries_regular_samples,
            input_range=self.base_block.input_range,
        )

    def _sim(self, inputs, time):
        baseout = self._base_block.sim(inputs, time)
        if self._augmentors:
            augout = []
            for aug in self._augmentors:
                augout.append(aug(inputs, time))
            augout = np.sum(augout)
            return augout + baseout
        else:
            return baseout

    def sim(self, input_signals, time_vector):
        return self._sim(input_signals, time_vector)


class ModelArray(Block):
    def __init__(self, models):
        assert all([model.input_dims == models[0].imput_dims for model in models])
        self.models = models
        self.parameters = np.stack([model.vectorized() for model in self.models])
        super(ModelArray, self).__init__(
            input_dim=models[0].input_dim,
            output_dim=models[0].output_dim,
            requires_regular_samples=True if any([model.requires_regular_samples for model in models]) else False,
            input_range=(np.min(np.vstack([np.vstack(model.input_range) for model in models]), axis=0),
                         np.max(np.vstack([np.vstack(model.input_range) for model in models]), axis=0)),
        )

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)

    def sim(self, volts, time):
        results = []
        for model in self.models:
            r = model.sim(volts, time)
            results.append(r)
        return np.stack(results, axis=0)

    def cv(self, data):
        assert len(data.shape) == 4
        return np.stack(self.cm(data), self.dm(data), axis=1)

    def dm(self, data):
        assert len(data.shape) == 4
        result = np.diff(data, axis=1) / data.shape[1]
        return result

    def cm(self, data):
        assert len(data.shape) == 4
        result = np.expand_dims(np.sum(data, axis=1) / data.shape[1], 1)
        return result


class RemoteHardwareMeasurement(Block):

    def __init__(self,
                 input_dim,
                 output_dim,
                 input_range,
                 measurement_host='kl1359-23:11234',
                 ):
        self.measurement_host = measurement_host

        super(RemoteHardwareMeasurement, self).__init__(
            input_dim=input_dim,
            output_dim=output_dim,
            input_range=input_range,
            requires_regular_samples=True,
        )
