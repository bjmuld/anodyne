import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import scale

from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection


class BootstrapPlots:
    def __init__(self):
        plt.ion()
        self.fig = plt.figure()
        self.ax = [
            self.fig.add_subplot(2, 2, 1, projection='3d'),
            self.fig.add_subplot(2, 2, 2, projection='3d'),
            self.fig.add_subplot(2, 2, 3),
            self.fig.add_subplot(4, 2, 6),
            self.fig.add_subplot(4, 2, 8),
        ]

    def do_plots(self, dprob1, dprob2, kld, error_data):

        for aa in self.ax:
            aa.cla()

        self.ax[0].scatter(error_data[:, 1], error_data[:, 0], dprob1, c=abs(error_data[:, 1]), s=50, cmap='cool')
        self.ax[1].scatter(error_data[:, 1], error_data[:, 0], dprob2, c=abs(error_data[:, 1]), s=50, cmap='cool')
        self.ax[2].plot(kld.cumsum() - kld[0])
        self.ax[0].set_title('old model histogram')
        self.ax[1].set_title('new model histogram')
        self.ax[2].set_title('KLD knowledge')

        self.ax[3].hist(error_data[:, 0], bins=60)
        self.ax[4].hist(error_data[:, 1], bins=60)
        self.ax[3].set_title('Sum Signal')
        self.ax[4].set_title('Difference Signal')
        plt.pause(1e-3)


class ErrorSpacePlots:

    def __init__(self, fignum):
            plt.ion()
            self.fig = plt.figure()
            self.ax = [self.fig.add_subplot(1, 1, 1)]
            self.ax[0].set_title('error histogram and mixture fit')
            self.errorboxes = None
            self.pc = None

    def plot(self, error_data, th_val, model, pdfs, scan, rois=None):
        # ax[1].plot(trialscore_f)
        # tsfmaxi = np.argmax(trialscore_f)
        # ax[1].plot(np.array([tsfmaxi, tsfmaxi]), np.array([trialscore_f.min(), trialscore_f.max()]), color='red')
        # ax[0].plot([th_val2, th_val2], [0, 1], color='red')

        if rois is not None:
            self.ax[0].cla()

            self.errorboxes = []

            # Loop over data points; create box from errors at each point
            for rowind in range(rois.shape[0]):
                rect = Rectangle((rois[rowind,0], 0), rois[rowind, :].ptp(), 1)
                self.errorboxes.append(rect)

            # Create patch collection with specified colour/alpha
            self.pc = PatchCollection(self.errorboxes, facecolor='green', alpha=0.5,
                                 edgecolor='green')

            # Add collection to axes
            self.ax[0].add_collection(self.pc)

        elif th_val is not None:
            self.ax[0].plot([th_val, th_val], [0, 1], color='black')
            self.ax[0].plot([th_val, error_data.max()], [1, 1], color='black')
            self.ax[0].plot([th_val, error_data.max()], [0, 0], color='black')
            self.ax[0].plot([error_data.max(), error_data.max()], [0, 1], color='black')
        else:
            raise RuntimeError('no roi or th_val supplied')

        for i in range(model.means_.shape[0]):
            self.ax[0].plot(
                scan,
                pdfs[i, :],
                color='red'
            )
        self.ax[0].hist(error_data, density=True, bins=100, color='teal')

        plt.show(block=False)
        plt.pause(1e-3)


class TSNEPlots:
    def __init__(self, tsne_data):
        plt.ion()
        self.fig = plt.figure()
        rn = np.ceil(np.sqrt(len(tsne_data)))
        self.ax = [
            self.fig.add_subplot(rn, rn, i + 1, projection='3d') for i in range(len(tsne_data))
        ]

    def plot(self, tsne_data, X):
        for i in range(len(tsne_data)):
            self.ax[i].scatter(tsne_data[i][0], tsne_data[i][1], tsne_data[i][2], c=X[:, -1].reshape(-1, ))
            plt.show()
            plt.pause(1e-3)
