import os
import csv
import numpy as np
import pkg_resources
from scipy.signal import firwin, lfilter, group_delay
from . import modeling
from . import prvs
import pyspectre
import waverunner
import subprocess
import h5py
#import xarray as xr

from pyspectre.server import run_batch as ps_run_batch

circuit_collateral_path = pkg_resources.resource_filename('anodyne', 'circuits')


def make_pwl_file(signal, time, filename):
    csv.register_dialect('spice_pwl', delimiter=' ')
    with open(filename, mode='w') as f:
        writer = csv.writer(f, dialect='spice_pwl')
        writer.writerows([(time[i], signal[i]) for i in range(time.size)])


def randomDiffInstBMPair(input_range, noise_color='brown'):
    models = []
    models.append(
        DiffInstAmp(
            gain=100,
            bandwidth=1e6,
            slew_rate=300e3,
            rails=(-12, 12),
            noise_mag=1e-6
        ))
    models.append(
        DiffInstAmp.from_vector(DiffInstAmp,
                                np.array([param + prvs.binorm(0.5 * param) for param in models[0].vectorized()]))
    )
    model = modeling.ModelArray(models)
    model.input_range = input_range
    model.noise_color = noise_color
    return model


class DCBlock(modeling.Block):
    def __init__(self):
        super(DCBlock, self).__init__()

    @staticmethod
    def sim(input, time=None):
        return (input.T - np.mean(input, axis=-1)).T


class LPFilter(modeling.Block):

    def __init__(self, order, cutoff, fs=None):
        self.order = order
        self.cutoff = cutoff
        self.fs = fs if fs else None
        if self.fs:
            self.coeffs = firwin(self.order, self.cutoff, fs=self.fs, pass_zero=True)
        self.requires_regular_samples()
        super(LPFilter, self).__init__()

    def sim(self, inputsig, time=None, fs=None):
        if time is not None:
            t_fs = 1 / np.mean(np.diff(time))
            if fs is None:
                fs = t_fs
            else:
                assert fs == t_fs
                fs = fs

        if fs and fs != self.fs:
            try:
                self.coeffs = firwin(self.order, self.cutoff, fs=fs, pass_zero=True)
                self.fs = fs
            except ValueError:
                return inputsig

        delay = int(np.floor(np.mean(group_delay((self.coeffs, 1.0))[1])))
        zeros = np.zeros((inputsig.shape[-1], delay)) if len(inputsig.shape) == 2 else np.zeros((delay,))
        inputsig = np.hstack([inputsig, zeros])
        return lfilter(self.coeffs, 1.0, inputsig).T[delay:].T


class Gain(modeling.Block):
    def __init__(self, gain, dc_offset=None, high_order_terms=None):
        self.gain = np.float(gain)
        self.dc = np.float(dc_offset) if dc_offset else 0.0
        self.high_order = np.array([high_order_terms]).flatten() if high_order_terms else None

        super(Gain, self).__init__()

    def sim(self, inputsig, time=None):
        if self.high_order:
            poly = np.append(self.high_order, self.gain)
            if self.dc:
                poly = np.append(poly, self.dc)
            return np.polyval(poly, inputsig)
        else:
            return self.gain*inputsig+self.dc


class Noise(modeling.Block):
    def __init__(self, mag, color):
        self.magnitude = np.float(mag)
        self.color = color
        assert self.color == 'white' or self.color == 'brown'
        super(Noise, self).__init__()

    def sim(self, inputsig, time=None):
        noise = self.magnitude * np.random.randn(*inputsig.shape)
        return inputsig+noise


class Clipping(modeling.Block):
    def __init__(self, limits):
        if np.size(limits) == 1:
            self.limits = np.array([-limits, limits])
        elif np.size(limits) == 2:
            self.limits = np.array(limits).flatten()
        super(Clipping, self).__init__()

    def sim(self, inputsig, time=None):
        return np.clip(inputsig, self.limits[0], self.limits[1])


class SlewLimit(modeling.Block):
    def __init__(self, limits):
        if np.size(limits) == 1:
            self.limits = np.array([-abs(limits), abs(limits)])
        elif np.size(limits) == 2:
            self.limits = np.array(limits).flatten()
        super(SlewLimit, self).__init__()

    def sim(self, inputsig, time):
        output_lim = np.array(inputsig)
        for i in range(1, len(output_lim)):
            if i > 0:
                ts = time[i]-time[i-1]
                slew_req = (inputsig[i] - output_lim[i - 1]) / ts
                if slew_req > self.limits[1]:
                    output_lim[i] = output_lim[i - 1] + ts * self.limits[1]
                    continue
                elif slew_req < self.limits[0]:
                    output_lim[i] = output_lim[i - 1] + ts * self.limits[0]
                    continue
                else:
                    output_lim[i] = output_lim[i-1] + ts * slew_req
        return output_lim


class AmpBM (modeling.Block):

    def __init__(self,
                 gain=100,
                 bandwidth=1e6,
                 filter_order=16,
                 slew_rate=(-np.Inf, np.Inf),
                 rails=(-np.Inf, np.Inf),
                 noise_mag=0.0,
                 ):

        self.gain = np.array(gain)
        self.bandwidth = np.float(bandwidth)
        self.filter_order = np.int(filter_order)
        self.slew_rate = np.array(slew_rate).flatten() if np.size(slew_rate) == 2 else np.array([-abs(slew_rate), abs(slew_rate)])
        self.rails = np.array(rails).flatten() if np.size(rails) == 2 else np.array([-abs(rails), abs(rails)])
        self.noise_mag = np.float(noise_mag)

        self.blocks = {
            'gain': Gain(self.gain),
            'noise': Noise(self.noise_mag),
            'filter': LPFilter(self.filter_order, self.bandwidth),
            'slewer': SlewLimit(self.slew_rate),
            'clipper': Clipping(self.rails),
        }

        super(AmpBM, self).__init__()

    def sim(self, volts, time):

        # apply linear gain
        output = self.blocks['gain'].sim(volts, time)

        # apply noise
        output = self.blocks['noise'].sim(output, time)

        # filter the signal
        output = self.blocks['filter'].sim(output, time)

        # slew limit
        output = self.blocks['slewer'].sim(output, time)

        # clip at rails
        output = self.blocks['clipper'].sim(output, time)

        return output


class InstAmpBM (modeling.Block):

    def __init__(self,
                 gain=100,
                 bandwidth=1e6,
                 filter_order=16,
                 slew_rate=(-np.Inf, np.Inf),
                 rails=(-np.Inf, np.Inf),
                 noise_mag=0.0,
                 ):

        self.gain = np.array(gain)
        self.bandwidth = np.float(bandwidth)
        self.filter_order = np.int(filter_order)
        self.slew_rate = np.array(slew_rate).flatten() if np.size(slew_rate) == 2 else np.array([-abs(slew_rate), abs(slew_rate)])
        self.rails = np.array(rails).flatten() if np.size(rails) == 2 else np.array([-abs(rails), abs(rails)])
        self.noise_mag = np.float(noise_mag)

        self.blocks = {
            'amp1': AmpBM(gain, bandwidth, filter_order, slew_rate, rails, noise_mag),
            'amp2': AmpBM(gain, bandwidth, filter_order, slew_rate, rails, noise_mag),
        }

        super(InstAmpBM, self).__init__(input_dim=2, output_dim=1)

    def sim(self, volts, time):
        # apply amp1
        output1 = self.blocks['amp1'].sim(volts[0], time)

        # apply amp2
        output = output1 - self.blocks['amp2'].sim(volts[1], time)

        return output


class DiffInstAmp (modeling.Block):

    def __init__(self,
                 gain=100,
                 bandwidth=1e6,
                 filter_order=16,
                 slew_rate=(-np.Inf, np.Inf),
                 rails=(-np.Inf, np.Inf),
                 noise_mag=0.0,
                 ):

        self.gain = np.array(gain)
        self.bandwidth = np.float(bandwidth)
        self.filter_order = np.int(filter_order)
        self.slew_rate = np.array(slew_rate).flatten() if np.size(slew_rate) == 2 else np.array([-abs(slew_rate), abs(slew_rate)])
        self.rails = np.array(rails).flatten() if np.size(rails) == 2 else np.array([-abs(rails), abs(rails)])
        self.noise_mag = np.float(noise_mag)

        self.blocks = {
            'amp1': InstAmpBM(gain, bandwidth, filter_order, slew_rate, rails, noise_mag),
            'amp2': InstAmpBM(gain, bandwidth, filter_order, slew_rate, rails, noise_mag),
        }

        super(DiffInstAmp, self).__init__(input_dim=2, output_dim=2)

    def sim(self, volts, time):
        # apply amp1
        output1 = self.blocks['amp1'].sim(volts, time)

        # apply amp2
        output2 = -self.blocks['amp2'].sim(volts, time)

        return np.stack([output1, output2])


class LDO_Pair (modeling.SpiceCircuitPair):

    def __init__(self, time_vector=None, **kwargs):

        inputs = {'line_input': 'V5',
                  'load_input': 'V3', }

        input_signal_names = ('line', 'load_gnd')

        paired_observations = [
            ('bm2_load', 'tx_load'),
            # ('net032', 'net039'),
            ('I17:out', 'I1:3'),
            ('I20:1', 'I23:1'),
            ('I20:3', 'I23:3'),
        ]

        super(LDO_Pair, self).__init__(
            netlist='new.scs',
            sim_directory=os.path.join(circuit_collateral_path, 'ldos-device-bm1'),
            pwl_inputs=inputs,
            input_range=((-3, -50e-3), (3, 500e-3)),
            input_bias=(0, 0),
            highpass_nodes=('bm2_load', 'tx_load'),
            input_signal_names=input_signal_names,
            observation_pairs=paired_observations,
            time_vector=time_vector,
            dtype='float32',
            **kwargs)


class RF_loopback(modeling.RemoteHardwareMeasurement):

    def __init__(self):
        super(RF_loopback, self).__init__(
            input_dim=2,
            output_dim=2,
            input_range=((-6, -6), (6, 6)),
        )

    def sim(self, volts, time):

        td = np.diff(time)
        assert td.ptp() < 0.01 * np.median(td)

        """
        remote_host, remote_fn_name, args=None, kwargs=None,
        password=default_password,
        thread_lock=None, event_timer=None,
        max_retries=default_max_challenge_retries,
        retry_interval=default_challenge_retry_interval,
        verbosity=default_verbosity
        """

        waves, caprate = waverunner.external_request(
            remote_host='kl1359-23:11234',
            remote_fn_name='stimcap.stimcap_single_records',
            kwargs={
                'generators_to_use': ['5412_A', '5412_B'],
                'digitizers_to_use': ['5105'],
                'stimuli_by_exp': volts,
                'stimulus_fs': 60e6,
                'capture_fs': 60e6,
                'optimize': True,
                'digi_coupling': ['DC', 'DC'],
                'digi_impedance': ['1M', '1M']
            },
            password='ratsofftoya',
        )

        n_chans = waves.shape[1] * waves.shape[2]
        return waves.reshape(waves.shape[0], n_chans, waves.shape[-1])















