//  VerilogA baseband behavioral model of a down-converting IQ mixer pair.
//  Copyright (c) 2000
//  by Cadence Design Systems, Inc.  All rights reserved.
 
// 1/6/00
 
/*
This is a simple passband model of an IQ demodulator. The local
oscillator is absorbed into this model.
*/
 
/* PARAMETER DEFINITIONS:
======================
I_gain    = available power gain in dB. 
I_IP3     = input referenced IP3(dBm)

Q_gain    = voltage gain in dB.
Q_IP3     = input referenced IP3(dBm)

nf      = noise figure [dB]
rin     = input resistance
rout    = output resistance

quad_error = quadrature error
flow       = local oscillator frequency
=====================
*/
 
`define PI 3.1415926535897932384626433

`include "constants.h"
`include "discipline.h"

module IQ_demodulator(in, I_out, Q_out, phase_err);
inout in ;
electrical in ;
inout I_out;
electrical I_out;
inout Q_out;
electrical Q_out;
inout phase_err;
electrical phase_err;

parameter real I_gain = 40;
parameter real Q_gain = 40;
parameter real I_ip3 = -30;
parameter real Q_ip3 = -30;
parameter real rin = 50 from (0:inf);
parameter real rout = 50 from (0:inf);

parameter real nf = 2 from [0:inf];
parameter real quad_error = 0;
parameter real flo = 1G;
 
real I_a;
real Q_a;
real I_b;
real Q_b;
real I_ip;
real Q_ip;
real rho;
real I_rhooutmax;
real Q_rhooutmax;
real I_rhoinmax;
real Q_rhoinmax;
real I_rhoout;
real Q_rhoout;
real theta;
real I_theta;
real Q_theta;
real tmp;
real noise_current;
real rnf;
real root2x2;
 
analog begin
 
// The initial block converts the input parameters from engineering
// units to implementation units.
  @( initial_step) begin

     I_a = sqrt(pow(10,I_gain/10)*rout/rin);
     I_ip = sqrt(pow(10,I_ip3/10)*2*rin*0.001);
     I_b = I_a/(I_ip*I_ip)*4/3;
     I_rhoinmax = sqrt(I_a/(3*I_b));
     I_rhooutmax = (2*I_a/3)*I_rhoinmax;

     Q_a = sqrt(pow(10,Q_gain/10)*rout/rin);
     Q_ip = sqrt(pow(10,Q_ip3/10)*2*rin*0.001);
     Q_b = Q_a/(Q_ip*Q_ip)*4/3;
     Q_rhoinmax = sqrt(Q_a/(3*Q_b));
     Q_rhooutmax = (2*Q_a/3)*Q_rhoinmax;
     Q_theta = 0;

     root2x2=2*sqrt(2);

     rnf = pow(10,nf/10);
     noise_current = sqrt(4*(rnf-1)*1.380620e-23*$temperature/rin);
 
   end

  rho = V(in);
 
// Apply the third order non-linearity. Clamp the
// output for extreme inputs.
  if ( abs(rho) < I_rhoinmax ) I_rhoout = (I_a - I_b*rho*rho)*rho;
  else if (rho < 0) I_rhoout = -I_rhooutmax;
  else I_rhoout = I_rhooutmax;
  if ( abs(rho) < Q_rhoinmax ) Q_rhoout = (Q_a - Q_b*rho*rho)*rho;
  else if (rho < 0) Q_rhoout = -Q_rhooutmax;
  else Q_rhoout = Q_rhooutmax;

 
// Adjust the angles.
  I_theta = 2*`PI*flo*($abstime)+V(phase_err); 

  I(phase_err) <+ V(phase_err)/50;
  I(in) <+ V(in)/rin;
  I(I_out) <+ (-root2x2*I_rhoout*cos(I_theta+quad_error/2) + V(I_out))/rout;
  I(Q_out) <+ (root2x2*Q_rhoout*sin(I_theta-quad_error/2) + V(Q_out))/rout;
 
  I(in) <+ white_noise(noise_current*noise_current, "IQ_demodulator");
 
end

endmodule
