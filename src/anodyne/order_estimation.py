import numpy as np
import matplotlib.pyplot as plt
plt.style.use(['ieee', 'seaborn-paper', 'seaborn-dark-palette'])
import xanity
import pywt

import concurrent.futures as FUT

import seaborn as sns

from scipy import stats
from scipy.spatial.distance import pdist, cdist
from scipy.spatial import ConvexHull
from scipy.optimize import minimize_scalar, Bounds
from scipy.signal import convolve, periodogram

from sklearn.neighbors import KernelDensity
from sklearn.preprocessing import scale

from .utilities import get_cv_response
from .circuits import LPFilter, DCBlock
from .stats_routines import centered

from . import modeling


def construct_x_get_dists(input_seq, output_seq, hypoth_memory):

    input_range = range(hypoth_memory, -1, -1)
    output_range = range(hypoth_memory,  0, -1)

    if hypoth_memory > 0:
        x = np.vstack([
            np.vstack([np.pad(input_seq,  ((0, 0), (i, 0)), 'constant')[:, :input_seq.shape[-1]] for i in input_range]),
            np.vstack([np.pad(output_seq, ((0, 0), (i, 0)), 'constant')[:, :output_seq.shape[-1]] for i in output_range]),
             ]).T
    else:
        x = input_seq.T

    return pdist(x, metric='seuclidean')


def get_entropic_edge(input_seq, output_dists, hypoth_memory, plot=False):
    x = np.array([np.pad(input_seq, (i, 0), 'constant')[:input_seq.size] for i in range(hypoth_memory, -1, -1)]).T

    pd_x = pdist(x, metric='cityblock')
    inds = np.split(np.argsort(pd_x), 2)[0]

    x = pd_x[inds]
    # x_w = ((x.max()-x)/(x.max()*x.shape[0])).sum()
    y = np.maximum.accumulate(output_dists[inds])
    # y_uc = np.count_nonzero(np.diff(y) != 0)

    # x_r = np.linspace(x[0], x[-1], 1000)
    # y_r = np.interp(x_r, x, y)
    # y_uc2 = np.count_nonzero(np.diff(y_r) != 0)

    if plot:
        plt.figure()
        plt.plot(y)
        plt.pause(0.1)

    # return y_r, x_w, y_uc, y_uc2
    return y


def get_entropic_edge2(input_seq, output_dists, hypoth_memory, plot=False):

    pd_x = construct_x_get_dists(input_seq, hypoth_memory)
    inds = np.array_split(np.argsort(pd_x), 2)[0]

    # x = pd_x[inds]
    # x_w = ((x.max()-x)/(x.max()*x.shape[0])).sum()
    y = np.maximum.accumulate(output_dists[inds])
    # y_uc = np.count_nonzero(np.diff(y) != 0)

    # x_r = np.linspace(x[0], x[-1], 1000)
    # y_r = np.interp(x_r, x, y)
    # y_uc2 = np.count_nonzero(np.diff(y_r) != 0)

    if plot:
        plt.figure('12344321')
        plt.plot(y)
        plt.pause(0.1)

    # return y_r, x_w, y_uc, y_uc2
    return y


def get_entropic_edge3(input_seq, output_seq, output_dists, hypoth_memory, plot=False):

    pd_x = construct_x_get_dists(input_seq, output_seq, hypoth_memory )
    inds = np.array_split(np.argsort(pd_x), 2)[0]

    # x = pd_x[inds]
    # x_w = ((x.max()-x)/(x.max()*x.shape[0])).sum()
    y = np.maximum.accumulate(output_dists[inds])
    # y_uc = np.count_nonzero(np.diff(y) != 0)

    # x_r = np.linspace(x[0], x[-1], 1000)
    # y_r = np.interp(x_r, x, y)
    # y_uc2 = np.count_nonzero(np.diff(y_r) != 0)

    if plot:
        plt.figure('12344321')
        plt.plot(y)
        plt.pause(0.1)

    # return y_r, x_w, y_uc, y_uc2
    return pd_x[inds]/pd_x[inds[-1]], y/y[-1]


def estimate_memory_dependence_test(system, mem_limit, minaction, maxaction, columnnames, plot=False, batch_size=None):
    i_seq = np.random.uniform(minaction, maxaction, (100 * mem_limit,))
    resp = system.run_eps(np.reshape(i_seq, (1, -1)), memory=0)
    sig_power = np.sum(get_cv_response(resp, columnnames) ** 2, axis=0)
    error_rat = sig_power[1] / (sig_power[0] / 2)
    xanity.log('error power: {}'.format(error_rat))

    output_dists = pdist(get_cv_response(resp, columnnames)[:, 1].reshape((-1, 1)))
    output_dists = output_dists / output_dists.max()

    edges = []
    scores = []

    for hypoth_memory in range(mem_limit):
        edge = get_entropic_edge(i_seq, output_dists, hypoth_memory)
        edges.append(edge)
        scores.append(np.count_nonzero(np.diff(edges[hypoth_memory])))
    scores = np.array(scores)
    # edges = np.vstack((np.ones(edges[0].shape), np.array(edges)))
    # del_edges = np.array([edges[i-1]-edges[i] for i in range(1, len(edges))])
    # scores = del_edges.clip(min=0).sum(axis=1) / weights

    alt_scores = np.zeros(shape=scores.shape)
    for i in range(scores.size):
        alt_dist = np.concatenate((scores[:i], scores[i + 1:]))
        alt_scores[i] = scores[i] - (np.mean(alt_dist) + 2 * alt_dist.std())

    pred = np.where(alt_scores == alt_scores.max())[0].flatten()[0] if len(np.where(alt_scores > 0)[0]) > 0 else -1

    if plot:
        fig = plt.figure(num=151515)
        ax1 = fig.add_subplot(1, 2, 1)
        ax2 = fig.add_subplot(1, 2, 2)
        # ax3 = fig.add_subplot(2, 3, 3)
        # ax4 = fig.add_subplot(2, 3, 4)
        # ax5 = fig.add_subplot(2, 3, 5)
        # ax6 = fig.add_subplot(2, 3, 6)

        ax1.plot(np.array(edges).T), ax1.set_title('edges')
        ax2.plot(scores), ax2.set_title('scores')
        # ax3.plot(sprmnr_n / sprmnr_o), ax3.set_title('new/old')
        # ax4.plot(sprmnr_o / sprmnr_n), ax4.set_title('old/new')
        # ax5.plot(sprmnr_n*(1-sprmnr_o)), ax5.set_title('new*(1-old')
        # ax6.plot(sprmnr_o*(1-sprmnr_n)), ax6.set_title('old*(1-new')

        plt.pause(1e-3)

    return pred, scores, None


def estimate_memory_impulse_trials(system, mem_limit, minaction, maxaction, batch_size=30, plot=False, p_thresh=0.05):
    alldata = np.empty((0, 2 * mem_limit + 1))
    old_model = None
    kde_model = None

    kld_trace = np.empty((0,))
    outscores = np.empty((0, mem_limit + 1))

    batch_ind = -1
    while True:
        batch_ind += 1
        batch_data = []
        for exp_no in range(batch_size):
            i_seq1 = np.random.uniform(minaction, maxaction, (2 * mem_limit + 1,))
            i_seq2 = np.array(i_seq1)
            i_seq2[mem_limit] = np.random.uniform(low=minaction, high=maxaction)

            resp1 = system.run_eps(np.reshape(i_seq1, (1, -1)))
            resp2 = system.run_eps(np.reshape(i_seq2, (1, -1)))
            bug_imp = get_cv_response(resp2)[:, 1] - get_cv_response(resp1)[:, 1]
            batch_data.append(abs(np.roll(bug_imp, -mem_limit)))

        batch_data = np.array(batch_data)
        alldata = np.concatenate((alldata, batch_data))

        alldata_s = alldata / alldata.std()
        null = alldata_s[:, mem_limit + 1:].flatten()
        subj = alldata_s[:, :mem_limit + 1]

        outscores = np.vstack((outscores, np.array([stats.ks_2samp(subj[:, entry].flatten(), null)[1]
                                                    for entry in range(subj.shape[1])])))

        if outscores.shape[0] > 10:

            if kde_model is not None:
                old_model = kde_model
            os_s = scale(outscores, axis=1)
            kde_model = [KernelDensity().fit(os_s[:, slot].reshape((-1, 1))) for slot in range(os_s.shape[1])]

            if old_model is not None:
                scan = np.array([np.linspace(
                    os_s[:, slot].min(axis=0) - 0.1 * os_s[:, slot].ptp(axis=0),
                    os_s[:, slot].max(axis=0) + 0.1 * os_s[:, slot].ptp(axis=0),
                    100) for slot in range(os_s.shape[1])])
                pdf1 = np.array([np.exp(kde_model[slot].score_samples(scan[slot, :].reshape(-1, 1))) for slot in
                                 range(os_s.shape[1])])
                pdf2 = np.array([np.exp(old_model[slot].score_samples(scan[slot, :].reshape(-1, 1))) for slot in
                                 range(os_s.shape[1])])
                kld_trace = np.concatenate([
                    kld_trace,
                    np.sum([
                        np.mean([stats.entropy(pdf1[slot], pdf2[slot]), stats.entropy(pdf2[slot], pdf1[slot])])
                        for slot in range(os_s.shape[1])
                    ]).reshape(-1)
                ])

                if plot:
                    plt.figure(num=141444)
                    plt.plot(kld_trace.cumsum())
                    plt.pause(1e-3)

                if np.all((kld_trace / kld_trace.cumsum())[-5:] < 0.02):
                    pred = np.where(outscores[-1] + outscores.std(axis=0) < p_thresh)[0][-1] \
                        if np.where(outscores[-1] + outscores.std(axis=0) < p_thresh)[0].size > 0 else -1
                    return pred, outscores[-1], kld_trace


def estimate_memory_impulse_trials_new(system, ts, mem_limit, minaction, maxaction, batch_size=30, plot=False, p_thresh=0.01):
    assert batch_size >= 10, 'must use batch size > 10 for KDE'
    nt = (2 * mem_limit) + 1
    time_vec = np.append(0, np.cumsum(np.array([ts] * (nt - 1))))

    alldata = np.empty((0, nt))

    old_model = None
    kde_model = None
    pdf = None
    old_pdf = None
    kld_trace = np.empty((0,))

    batch_ind = -1
    while True:
        batch_ind += 1

        i_seq1 = np.random.uniform(minaction, maxaction, size=(batch_size, nt))
        i_seq2 = np.array(i_seq1)
        i_seq2[:, mem_limit] = np.random.uniform(minaction, maxaction, (batch_size,))

        resp1 = system.run(i_seq1, time_vec)[0]
        resp2 = system.run(i_seq2, time_vec)[0]
        bug_imp = abs(system.dm(resp2) - system.dm(resp1))
        batch_data = np.roll(bug_imp, mem_limit + 1)
        alldata = np.vstack((alldata, batch_data))

        # do jackknife over all_data and build model over test results
        alldata_s = alldata / alldata.std()
        bs_scores = []
        for i in range(alldata_s.shape[0]):
            bs = np.vstack((alldata_s[:i, :], alldata_s[i + 1:, :]))
            null = bs[:, mem_limit:].flatten()
            subj = bs[:, :mem_limit]

            scores = []
            for column in subj.T:
                scores.append((stats.ks_2samp(column.flatten(), null)[1]))
            bs_scores.append(np.hstack(scores))

        bs_scores = np.stack(bs_scores)

        if kde_model is not None:
            old_model = kde_model
            old_pdf = pdf

        # if np.isinf(np.sum(bs_scores)):
        #     min_score = np.min(bs_scores[bs_scores != -np.Inf])
        #     bs_scores[bs_scores == - np.Inf] = min_score
        #     max_score = np.max(bs_scores[bs_scores != np.Inf])
        #     bs_scores[bs_scores == np.Inf] = max_score

        # bs_scores = scale(bs_scores, axis=1, with_mean=False, with_std=True)

        kde_model = [KernelDensity().fit(column.reshape((-1, 1))) for column in bs_scores.T]
        # kde_model = [stats.gaussian_kde(column.reshape((-1, 1))) for column in bs_scores.T]

        scan = np.stack([np.linspace(
            column.min(axis=0) - 0.1 * column.ptp(axis=0),
            column.max(axis=0) + 0.1 * column.ptp(axis=0),
            100) for column in bs_scores.T])

        pdf = np.array([
            np.exp(model.score_samples(scan[i].reshape(-1, 1)))
            # model.pdf(scan[i].reshape(-1,))
            for i, model in enumerate(kde_model)
        ])

        if old_model is not None:

            kld_trace = np.concatenate([
                kld_trace,
                np.sum([
                    # np.mean([stats.entropy(dist1, dist2), stats.entropy(dist1, dist2)])
                    stats.entropy(dist1, dist2)
                    for dist1, dist2 in zip(pdf, old_pdf)
                ]).reshape(-1)
            ])

            if plot:
                plt.figure(num=141444)
                plt.plot(kld_trace.cumsum())
                plt.pause(1e-3)

            if np.all((kld_trace / kld_trace.cumsum())[-3:] < 0.02):
                skore = np.mean(bs_scores, axis=0)
                skore_margin = skore + 3*bs_scores.std(axis=0)
                pred = np.where(skore_margin < p_thresh)[0]
                pred = pred[-1] if pred.size > 0 else -1
                return pred, skore, kld_trace


def estimate_memory_dependence_new(system, ts, mem_limit, minaction, maxaction, plot=False, obs_dur=10000):
    time = np.arange(0, obs_dur*ts, ts)
    i_seq = np.random.uniform(minaction, maxaction, (1, time.size))
    resp = system.run(i_seq, time)[0]
    dm_resp = system.dm(resp)
    cm_resp = system.cm(resp)
    dm_power = np.sum(dm_resp ** 2)
    cm_power = np.sum(cm_resp ** 2)
    error_rat = dm_power / cm_power
    xanity.log('error power: {}'.format(error_rat))

    output_dists = pdist(dm_resp.T, metric='seuclidean')
    output_dists = output_dists / output_dists.max()

    edges = []
    scores = []
    for hypoth_memory in range(mem_limit):
        edge = get_entropic_edge2(i_seq, dm_resp, output_dists, hypoth_memory)
        edges.append(edge/edge[-1])
        scores.append(np.sum(edge))
    scores = np.array(scores)

    # edges = np.vstack((np.ones(edges[0].shape), np.array(edges)))
    # del_edges = np.array([edges[i-1]-edges[i] for i in range(1, len(edges))])
    # scores = del_edges.clip(min=0).sum(axis=1) / weights

    alt_scores = np.zeros(shape=scores.shape)
    for i in range(scores.size):
        alt_dist = np.concatenate((scores[:i], scores[i + 1:]))
        alt_scores[i] = scores[i] - (np.mean(alt_dist) + 2 * alt_dist.std())

    pred = np.where(alt_scores == alt_scores.min())[0][0]

    if plot:
        fig = plt.figure(num=151515)
        ax1 = fig.add_subplot(1, 2, 1)
        ax2 = fig.add_subplot(1, 2, 2)
        # ax3 = fig.add_subplot(2, 3, 3)
        # ax4 = fig.add_subplot(2, 3, 4)
        # ax5 = fig.add_subplot(2, 3, 5)
        # ax6 = fig.add_subplot(2, 3, 6)

        ax1.plot(np.array(edges).T), ax1.set_title('edges')
        ax2.plot(scores), ax2.set_title('scores')
        # ax3.plot(sprmnr_n / sprmnr_o), ax3.set_title('new/old')
        # ax4.plot(sprmnr_o / sprmnr_n), ax4.set_title('old/new')
        # ax5.plot(sprmnr_n*(1-sprmnr_o)), ax5.set_title('new*(1-old')
        # ax6.plot(sprmnr_o*(1-sprmnr_n)), ax6.set_title('old*(1-new')

        plt.pause(1e-3)

    return pred, scores, None


def estimate_memory_dependence_correlation(system, ts, mem_limit, plot=False, batch_size=50, obs_dur=10000):

    time = np.arange(0, obs_dur*ts, ts)
    i_seq = system.gen_random_inputs(batch_size, time.size)
    resp = system.run(i_seq, time)[0]

    dm_resp = system.dm(resp)
    cm_resp = system.cm(resp)
    dm_power = np.sum(dm_resp ** 2)
    cm_power = np.sum(cm_resp ** 2)
    error_rat = dm_power / cm_power
    xanity.log('error power: {}'.format(error_rat))

    output_dists = np.stack([pdist(obs.reshape((-1, time.size)).T, metric='seuclidean') for obs in dm_resp])
    output_dists = (output_dists.T / output_dists.max(axis=1)).T

    wt = np.logspace(0, -1, int(np.ceil(output_dists[0].size / 2)))
    scores = np.zeros((batch_size, mem_limit))
    edges = []

    for exp in range(batch_size):
        obs = dm_resp[exp, 0, :, :].reshape(-1, time.size)

        for i, hypoth_memory in enumerate(range(mem_limit)):
            _, edge = get_entropic_edge3(i_seq[exp, :, :], obs, output_dists[exp], hypoth_memory)
            edges.append(edge* wt)
            scores[exp, i] = np.sum(edge * wt)

    stats = np.vstack([np.mean(scores, axis=0), np.std(scores, axis=0)]).T

    alt_scores = []
    for i in range(stats.shape[0]):
        altern = np.vstack([stats[:i], stats[i+1:]])
        altern = altern[:, 0] - altern[:, 1]
        alt_scores.append(altern.min() - stats[i][0]+stats[i][1])

    alt_scores = np.stack(alt_scores)
    pred = np.where(alt_scores == alt_scores.max())[0][0]

    if plot:
        fig = plt.figure(num=151515)
        ax1 = fig.add_subplot(1, 3, 1)
        ax2 = fig.add_subplot(1, 3, 2)
        ax3 = fig.add_subplot(1, 3, 3)
        # ax4 = fig.add_subplot(2, 3, 4)
        # ax5 = fig.add_subplot(2, 3, 5)
        # ax6 = fig.add_subplot(2, 3, 6)

        ax1.plot(np.array(edges).T), ax1.set_title('edges')

        plt.sca(ax2)

        # ## Use cubehelix to get a custom sequential palette
        pal = sns.cubehelix_palette(mem_limit, rot=-.5, dark=.3)
        # # Show each distribution with both violins and points
        sns.violinplot(data=scores, palette=pal, inner="points")
        plt.title('integrals')

        ax3.plot(alt_scores), ax3.set_title('scores')
        # ax3.plot(sprmnr_n / sprmnr_o), ax3.set_title('new/old')
        # ax4.plot(sprmnr_o / sprmnr_n), ax4.set_title('old/new')
        # ax5.plot(sprmnr_n*(1-sprmnr_o)), ax5.set_title('new*(1-old')
        # ax6.plot(sprmnr_o*(1-sprmnr_n)), ax6.set_title('old*(1-new')

        plt.pause(1e-3)

    return pred, scores, None

def scan_timescales(
        system,
        exp_len=100,
        ts_min=1e-12,
        ts_max=1,
        ts_res=0.5,
        batch_size=30,
        oversampling=2,
):
    nt = exp_len
    i_seq = system.gen_random_inputs(batch_size, nt, color='mixed', amplitudes='log', oversampling=oversampling)
    tts = 10 ** np.linspace(np.log10(ts_min), np.log10(ts_max), 1 + int((np.log10(ts_max) - np.log10(ts_min)) / ts_res))
    time_keys = [ts.astype(str) for ts in tts]
    times = []
    time_index = []
    for ts, time_key in zip(tts, time_keys):
        times.extend([np.r_[0, np.cumsum(np.array([ts] * (nt - 1)))]] * batch_size)
        time_index.extend([time_key] * batch_size)

    times = np.array(times)
    i_seq = np.broadcast_to(np.expand_dims(i_seq, 0), (tts.size, *i_seq.shape)).reshape(tts.size * i_seq.shape[0],
                                                                                        *i_seq.shape[1:])

    r_times, responses = system.sim_batch(i_seq, times, strobe=True, resample=False)

    databases = {key: [] for key in time_keys}
    for time_key, time, result in zip(time_index, r_times, responses):
        databases[time_key].append(result)

    return databases


def optimal_timescale(
        system,
        exp_len=100,
        ts_min=1e-12,
        ts_max=1,
        ts_res=0.5,
        batch_size=30,
        scan=False,
        oversampling=2,
        plot=False
):

    def measure_block(resp):
        relmags = resp.cm.flat_signals.std(axis=0)
        relmags = relmags/relmags.max()

        dm = resp.dm/relmags
        cm = resp.cm/relmags

        dm_spect = periodogram(dm, detrend='constant', axis=1, scaling='spectrum')[1]
        cm_spect = periodogram(cm, detrend='constant', axis=1, scaling='spectrum')[1]

        dm_lf_pow = np.sum(dm_spect[:, :-1, :])
        dm_hf_pow = np.sum(dm_spect[:, -1, :])
        cm_lf_pow = np.sum(cm_spect[:, :-1, :])
        cm_hf_pow = np.sum(cm_spect[:, -1, :])

        return dm_hf_pow, dm_lf_pow, cm_hf_pow, cm_lf_pow

    def penalty(metrics):
        dm_hf_pow, dm_lf_pow, cm_hf_pow, cm_lf_pow = metrics

        penalty = 100*dm_hf_pow - dm_lf_pow

        if scan:
            return metrics + tuple([penalty])
        else:
            return penalty

    if scan:

        databases = scan_timescales(
            system,
            exp_len=exp_len,
            ts_min=ts_min,
            ts_max=ts_max,
            ts_res=ts_res,
            batch_size=batch_size,
            oversampling=oversampling,
        )

        tts = np.array(databases.keys())
        tts.sort()

        metrics = {}
        for key in databases.keys():
            databases[key] = system.new_database().append_data(np.stack(databases[key]))
            _metrics = measure_block(databases[key])
            metrics[key] = penalty(_metrics)

        if plot:
            allmetrics = np.stack([metrics[key] for key in databases.keys()]).T
            items = ('dm_hf_pow', 'dm_lf_pow', 'cm_hf_pow', 'cm_lf_pow', 'penalty')
            f, ax = plt.subplots(len(items), 1)
            for i, label in enumerate(items):
                ax[i].semilogx(tts, allmetrics[i], label=label)
                ax[i].set_ylabel(label)
            plt.xlabel('Timestep Size')

        return metrics

    else:
        return 10 ** minimize_scalar(
            lambda ts: penalty(get_relpow(ts)),
            method='Bounded',
            bounds=(np.log10(ts_min), np.log10(ts_max)),
            tol=0.001,
            options={'disp': True},
        )['x']


def get_optimal_obs_len(system, ts, memory_estimate, minaction, maxaction, batch_size, max_obs_dur=500):

    time = np.arange(0, max_obs_dur * ts, ts)
    i_seq = np.random.uniform(minaction, maxaction, (max_obs_dur, time.size))
    resp = system.cm(system.run(i_seq, time)[0])

    plt.plot(np.var(resp, axis=0))

    raise NotImplementedError


def get_isolation(point_index, _x_data, _y_data, n_neighbors):
    n_points = _x_data.shape[0]
    print_interval = int(n_points / 20)

    point = _x_data[point_index]
    dists = cdist(point.reshape(1, -1), _x_data, metric='cityblock').squeeze()
    neighbors = np.argsort(dists)[:n_neighbors].astype(int)
    neighbors = neighbors[neighbors != point_index]
    
    result = np.median(cdist(_y_data[point_index].reshape(1, -1), _y_data[neighbors]))

    if not point_index % print_interval:
        print("{}% done...".format(100 * point_index / n_points))

    return result


def estimate_system_memory_from_data(system, min_hypoth_mem=0, max_hypoth_mem=10, n_neighbors=99):
    min_hypoth_mem = int(min_hypoth_mem)
    max_hypoth_mem = int(max_hypoth_mem)

    scores = []

    for hyp in range(min_hypoth_mem, max_hypoth_mem):

        _x_data = system.database.in_cm.scaled.to_states(hyp).transitions.data[0]
        _y_data = system.database.cm.scaled.to_states(hyp).transitions.data[1]
        point_inds = list(range(_x_data.shape[0]))
        
        with FUT.ThreadPoolExecutor(max_workers=16) as POOL:            

            results = POOL.map(get_isolation, point_inds, [_x_data]*_x_data.shape[0], [_y_data]*_x_data.shape[0], [n_neighbors]*_x_data.shape[0])

        del _x_data, _y_data
        scores.append(np.stack([res for res in results]))

    scores = np.median(np.stack(scores), axis=1)

    return int(np.where(scores == np.min(scores))[0][0])


def estimate_max_simtime(system, ts_min=1e-12, ts_max=1, batch_size=300, n_points=109,
                         order_resolution=0.5, debug=False, plot=False):

    if debug:
        batch_size = 17
        n_points = 60

    sigpack = system.gen_random_inputs(batch_size, length=n_points, oversampling=5)

    init_exp = np.log10(ts_min)
    fin_exp = np.log10(ts_max)
    count = int(np.round((fin_exp-init_exp)/order_resolution))
    tss = np.logspace(np.log10(ts_min), np.log10(ts_max), count)

    xanity.log('about to scan {} timesteps.'.format(len(tss)))

    for i, ts in enumerate(tss):
        xanity.log('starting timestep {}/{}...'.format(i, len(tss)))
        time_vec = np.cumsum([0] + [ts, ] * (sigpack.shape[2] - 1))
        system.database.append_data(system.sim_batch(sigpack, time_vec))

    bloc = system.database.dm.scaled

    power = np.zeros(len(tss))
    for i, batch_inds in enumerate(np.array_split(np.arange(count*batch_size), count)):

        subj = bloc.indices(batch_inds)
        cmat = np.cov(subj.swapaxes(0, 1).reshape(subj.shape[1], -1))
        power[i] = np.trace(cmat)

    result = tss[np.where(power == power.max())[0][0]]

    if plot:
        plt.semilogx(tss, power)
        plt.title("LDO Differential Observation Power v.\nObservation Duration ({} obs. x {} dims., fixed Ts/Fs)"
                  .format(bloc.shape[0]*bloc.shape[2], bloc.shape[1]))
        plt.xlabel('Observation Timescale (s)')
        plt.ylabel('Observation Power')

    return result




