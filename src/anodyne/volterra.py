import numpy as np
import pickle
from itertools import combinations_with_replacement as cwr
from scipy.special import comb
from xanity import persistent

from .prvs import binorm
import gym.spaces as gs

seed_filename = 'seed.pkl'


def fmap(state, action, params):
    order = params.shape[0] - 1
    output = params[0, 0]
    for o in range(order):
        ostack = np.array([np.product(tupe) for tupe in cwr(np.hstack((state, action)), o + 1)])
        output += np.dot(ostack, params[o + 1, 0:len(ostack)])
    return np.clip(output, -1, 1)


class Model:
    def __init__(self,
                 sp_nlorder=4,
                 sp_memory=4,
                 sp_noise_scale=6e-4,
                 sp_nbigpurturbs=2,
                 sp_bigpurturbsize=2e-1,
                 sp_nsmallpurturbs=10,
                 sp_smallpurturbsize=2e-3,
                 input_range=np.array([[-1, 1]]),
                 ):

        np.random.seed(15156)

        self.observation_space = gs.Box(low=np.array([-1, -1]), high=np.array([1, 1]), dtype='float64')
        self.action_space = gs.Box(low=np.array(input_range[:, 0]), high=np.array(input_range[:, 1]), dtype='float64')
        self.input_range = input_range

        nparams = 1
        for i in range(sp_nlorder):
            nparams += comb(sp_memory + 1, i + 1, repetition=True, exact=True)

        params1 = np.random.randn(nparams)
        params1 = params1 / abs(abs(params1).sum())
        params2 = np.copy(params1)

        bigperturbinds = np.random.randint(0, params1.size, sp_nbigpurturbs)
        smallperturbinds = np.random.randint(0, params1.size, sp_nsmallpurturbs)

        params2[bigperturbinds] = binorm(
            mu1=params2[bigperturbinds]+sp_bigpurturbsize*abs(params2[bigperturbinds]),
            mu2=params2[bigperturbinds]-sp_bigpurturbsize*abs(params2[bigperturbinds]),
            shape=bigperturbinds.shape
        )

        params2[smallperturbinds] = binorm(
            mu1=params2[smallperturbinds]+sp_smallpurturbsize*abs(params2[smallperturbinds]),
            mu2=params2[smallperturbinds]-sp_smallpurturbsize*abs(params2[smallperturbinds]),
            shape=smallperturbinds.shape
        )
        #params2 = params2 / abs(abs(params2).sum())

        self.subsys1 = System(fmap, params1, sp_memory, sp_nlorder, noise_scale=sp_noise_scale)
        self.subsys2 = System(fmap, params2, sp_memory, sp_nlorder, noise_scale=sp_noise_scale)
        self.sys = System(
            lambda state, action, params:
                np.hstack(
                    (
                        100.*self.subsys1.step(action, state=state),
                        100.*self.subsys2.step(action, state=state)
                    )),
            None,
            sp_memory,
            sp_nlorder,
        )
        self.bigperturbinds = bigperturbinds
        self.smallperturbinds = smallperturbinds
        self.bigperturbsize = sp_bigpurturbsize
        self.smallperturbsize = sp_smallpurturbsize
        self.params1 = structure_params(params1, sp_memory, sp_nlorder)
        self.params2 = structure_params(params2, sp_memory, sp_nlorder)

    # def __getattr__(self, item):
    #     return getattr(self.sys, item)

    def step(self, *args, **kwargs):
        return self.sys.step(*args, **kwargs)

    def reset(self, *args, **kwargs):
        return self.sys.reset(*args, **kwargs)

    def steps(self, *args, **kwargs):
        return self.sys.steps(*args, **kwargs)


class DiscreteModel(Model):

    def __init__(self, *args, **kwargs):
        super(DiscreteModel, self).__init__(*args, **kwargs)
        self.discretization = 64
        self.lsb = np.ptp(self.input_range, axis=1)/self.discretization
        self.base = np.min(self.input_range, axis=1)
        self.action_space = gs.Discrete(self.discretization-1)

    def step(self, action):
        action = self.base + action * self.lsb
        return super(DiscreteModel, self).step(action)

    def steps(self, actions, *args, **kwargs):
        actions = self.base + actions * self.lsb
        return super(DiscreteModel, self).steps(actions, *args, **kwargs)


class GymWrapper(object):
    def __init__(self, model, reward_fn=None, episode_len=0):
        self.model = model
        self.last_response = np.zeros(self.model.observation_space.low.shape)
        self.reward_fn = reward_fn
        self.episode_len = episode_len
        self.step_ct = 0
        self.best_ever = 0
        self.best_step = 0

    def reset(self):
        self.model.reset()
        self.step_ct = 0
        self.last_response = np.zeros(self.model.observation_space.low.shape)
        self.best_step = 0

    def step(self, input):
        response = self.model.step(input)
        reward = self.reward_fn(self, self.last_response, response)
        if reward > self.best_step:
            self.best_step = reward
        if reward > self.best_ever:
            self.best_ever = reward
        self.last_response = response
        self.step_ct += 1

        if self.episode_len == 0:
            done = False
        else:
            done = False if 0 <= self.step_ct < self.episode_len else True

        return response, reward, done, None

    def steps(self, actions):

        ep_res = []
        ep_rew = []
        ep_done = []
        ep_info = []

        for test_no, test_vec in enumerate(actions):

            self.reset()
            res = []
            rew = []
            info = []

            for step_no, step_vec in enumerate(test_vec):

                response, reward, done, inffo = self.step(step_vec)

                res.append(response)
                rew.append(reward)
                info.append(inffo)

                if done:
                    ep_done.append(step_no)
                    break

            ep_done.append(step_no)
            ep_res.append(np.stack(res))
            ep_rew.append(np.stack(rew))
            ep_info.append(np.stack(info))

        return np.stack(ep_res), np.stack(ep_rew), np.stack(ep_done), np.stack(ep_info)

    def __getattr__(self, item):
        return getattr(self.model, item)


class System:

    def __init__(self, f_map, params, memory=3, order=3, noise_scale=0.):

        self.fmap = f_map
        self.state = None
        self.memory = memory
        self.nlorder = order
        self.noise_scale = noise_scale

        if params is not None:
            self.params = structure_params(params, memory, order)
        else:
            self.params = None

    def step(self, action, state=None):
        if state is not None:
            self.state = state
        output = self.fmap(self.state, np.clip(action, -1, 1), self.params)
        self.state = np.hstack((self.state[1:], action))
        return output + np.random.randn() * self.noise_scale

    def reset(self):
        self.state = np.zeros(self.memory)

    # def steps(self, inputs, memory=0):
    #     tdata = np.zeros((inputs.shape[0], inputs.shape[1], memory + 3))
    #
    #     for epno, epvec in enumerate(inputs):
    #         epdata = np.zeros((epvec.size, memory + 3))
    #         self.reset()
    #         for i, step in enumerate(epvec):
    #             oldstate = np.pad(epvec[i-memory:i], (memory, 0), mode='constant')\
    #                 if np.size(epvec[i-memory:i]) < memory else epvec[i-memory:i]
    #             output = self.step(step)
    #             epdata[i, :] = np.hstack([oldstate, step, output])
    #         tdata[epno, :, :] = epdata
    #
    #     return np.vstack([tdata[i, :, :] for i in range(tdata.shape[0])])


def structure_params(params, memory, order):
    if type(params) is not np.ndarray:
        params = np.array(params)
    if len(params.shape) == 2 and np.min(params.shape) > 1:
        return params
    if len(params.shape) is not 2 and memory and order:
        params = params.flatten()
        oparams = np.zeros((order + 1, comb(memory + 1, order, repetition=True, exact=True)))
        oparams[0, 0] = params[0]
        params = params[1:]
        for i in range(order):
            maxind = comb(memory + 1, i + 1, repetition=True, exact=True)
            oparams[i + 1, 0:maxind] = params[0:maxind]
            params = params[maxind:]
    else:
        oparams = params
    return oparams

