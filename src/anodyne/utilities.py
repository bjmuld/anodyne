import os
import pickle
import pandas as pd
import numpy as np
from logging import getLogger
import xanity

from xanity import load_checkpoint, save_checkpoint
from itertools import combinations_with_replacement as cwr


#####from ..qlsamp import tsnefilename, bootstrapfilename, kde_model_filename, hypersample_filename, transformer_filename
bootstrapfilename = 'bootstrap.hdf5'
gmm_models_filename = 'error_gmms.pkl'
kde_model_filename = 'kd_model.pkl'
hypersample_filename = 'hypdata.hdf5'
transformer_filename = 'transformers.pkl'
tsnefilename = 'tsne.hdf5'


def load_kde_model():
    file = xanity.load_checkpoint(kde_model_filename)
    if file:
        return pickle.load(open(file, 'rb'))
    else:
        return None


def save_kde_model(kde_model):
    with open(xanity.save_checkpoint(kde_model_filename), 'wb') as f:
        pickle.dump(kde_model, f, protocol=pickle.HIGHEST_PROTOCOL)


def load_bootstrap():
    file = xanity.load_checkpoint(bootstrapfilename)
    if file:
        return pd.read_hdf(file, 'table', format='table')
    else:
        return None


def save_bootstrap(data):
    data.to_hdf(save_checkpoint(bootstrapfilename), 'table', format='table', dropna=True)


def load_hypersample_data():
    
    if xanity.load_checkpoint(hypersample_filename):
        hyp_data = pd.read_hdf(load_checkpoint(hypersample_filename), 'table', format='table')
    else:
        hyp_data = None

    if xanity.load_checkpoint(transformer_filename):
        with open(load_checkpoint(transformer_filename), 'rb') as f:
            transformer = pickle.load(f)
    else:
        transformer = None

    return hyp_data, transformer


def save_hypersample_data(hyp_data, transformer):
    hyp_data.to_hdf(save_checkpoint(hypersample_filename), 'table', format='table', dropna=True)
    with open(save_checkpoint(transformer_filename), 'wb') as f:
        pickle.dump(transformer, f, protocol=pickle.HIGHEST_PROTOCOL)


def load_tsne_data():
    if os.path.isfile(os.path.join(metadata['persist_dir'], tsnefilename)):
        with pd.HDFStore(os.path.join(metadata['persist_dir'], tsnefilename)) as store:
            maps = []
            names = store.keys()
            for item in names:
                maps.append(store.get(item))
        return maps


def save_tsne_data(tsne_data):
    savepath = save_checkpoint(tsnefilename)
    for i in range(len(tsne_data)):
        tsne_data[i].to_hdf(
            savepath,
            'table' + str(i),
            format='table',
            dropna=True)


def get_columnnames(memory):
    return ['state' + str(i) for i in range(memory)] + ['action'] + ['output' + str(i) for i in range(2)] +\
           ['augmented-output', 'inner-iteration', 'outer-iteration']


def get_stimnames(memory):
    return get_columnnames(memory)[0:memory+1]


def get_stims(data, columnnames, stimnames):
    if type(data) is pd.DataFrame or type(data) is pd.Series:
        return data[stimnames].values
    elif type(data) is np.ndarray:
        if len(columnnames) == data.shape[1]:
            return data[:, [col in stimnames for col in columnnames]]


def get_error(data, columnnames):
    if type(data) is pd.DataFrame:
        return (data['output0'] - data['augmented-output']).values.reshape((-1, 1))
    elif type(data) is pd.Series:
        return (data['output0'] - data['augmented-output']).reshape((-1, 1))
    else:
        if len(columnnames) == data.shape[1]:
            return (data[:, [col == 'output0' for col in columnnames]] - data[:, [col == 'augmented-output' for col in columnnames]]).reshape((-1 ,1))


def get_cv_response(data, columnnames):
    if type(data) is pd.DataFrame:
        return np.array(
            ((data['output0'] + data['augmented-output']).values/2,
             (data['output0'] - data['augmented-output']).values/2)
        ).T
    elif type(data) is pd.Series:
        return np.array(
            [(data['output0'] + data['augmented-output'])/2,
             (data['output0'] - data['augmented-output'])/2]
        ).reshape((-1, 2))
    else:
        if len(columnnames) == data.shape[1]:
            return np.hstack((
                (data[:, [col == 'output0' for col in columnnames]] + data[:, [col == 'augmented-output' for col in columnnames]])/2,
                (data[:, [col == 'output0' for col in columnnames]] - data[:, [col == 'augmented-output' for col in columnnames]])/2
             )).reshape((-1, 2))
        else:
            return np.vstack((
                (data[:, -2] + data[:, -1])/2,
                (data[:, -2] - data[:, -1])/2
             )).T


def data_to_cvdata(data, columnnames, stimnames):

    if type(data) is pd.DataFrame:
        return data[stimnames], get_cv_response(data)

    else:
        if len(columnnames) == data.shape[1]:
            return np.hstack((
                data[:, [col in stimnames for col in columnnames]],
                get_cv_response(data)
             ))


def sim_augmented_output(idata, augmentors, stimnames):

    # make initial assumption of augmentation
    if 'augmented-output' not in idata:
        idata['augmented-output'] = idata['output1']

    if not augmentors:
        return idata['output1'].values.reshape((-1, 1))

    else:
        output = idata['output1'].values

        for i, (model, transformer, desc, train_error) in enumerate(augmentors):
            x = np.divide(
                (
                        np.vstack((
                            transformer.transform(idata[stimnames].values).T,
                            # idata['output0'],
                            # idata['output1'],
                            output,
                        )).T - desc['x_loc']
                ),
                desc['x_std'],
                where=(desc['x_std'] != 0)
            )

            output = output + model.predict(x) * desc['y_std'] + desc['y_loc']

        return output.reshape((-1, 1))


def analyze_model(
        model,
        sp_nlorder=None,
        sp_memory=None,
        logger=None
):
    if logger is not None:
        mylogger = logger
    else:
        mylogger = getLogger()

    d_coeff = model.params1 - model.params2
    mem_locs = np.transpose(np.nonzero(d_coeff != 0.0))

    mem_map = [np.array([[-1]])]
    for i in range(1, sp_nlorder + 1):
        row = np.array(list(cwr(list(range(sp_memory, -1, -1)), i))).T.reshape((i, -1))
        if row.size > 0:
            mem_map.append(row)

    mem_record = [(list(mem_map[loc[0]][:, loc[1]]), d_coeff[loc[0], loc[1]]) for loc in mem_locs]
    mylogger.info('mem_record:  {}'.format(mem_record))
    return np.max(np.concatenate([rec[0] for rec in mem_record]))