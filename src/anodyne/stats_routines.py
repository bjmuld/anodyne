from scipy.stats.distributions import halfnorm
from scipy.stats import kstest
from numpy.random import choice
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#import xarray as xr
import h5py

import sklearn.mixture as skmix
from sklearn.linear_model import LinearRegression as LR
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.decomposition import PCA
from sklearn.neighbors import KernelDensity
from sklearn.manifold import TSNE

import scipy.stats as stats
import scipy.spatial.distance as Dist
import scipy.optimize as SPO
from scipy.spatial import cKDTree

from .plotting import ErrorSpacePlots, BootstrapPlots
from .utilities import *
from .modeling import PairedBatchDatabase

# import experiments.include.vae_cnn as vae

def centered(data, axis=-1):
    mean = np.mean(data, axis=axis)
    try:
        return data - mean
    except ValueError:
        try:
            return (data.T - mean).T
        except ValueError:
            try:
                return (data - mean.T)
            except ValueError:
                return (data.T - mean.T).T


def fit_halfgeneral(data, startind=None, stopind=None, indres=None, is_sorted=False, n_samples=200):
    startind = startind if startind is not None else 0
    stopind = stopind if stopind is not None else len(data)
    indres = int(round(indres)) if indres is not None else int(round((stopind - startind) / 20))

    if stopind <= 10:
        raise ValueError('can\'t search less than 10 samples')

    data_s = data if is_sorted else np.sort(data)
    inds = list(range(startind, stopind, indres))
    scores = []

    for i in inds:
        if i <= 10:
            continue
        data_sc = data_s[0:i]
        scores.append(np.exp(kstest(choice(data_sc.reshape(-1), n_samples), 'halfnorm', args=halfnorm.fit(data_sc))[0]))

    plt.plot(scores)
    plt.show(block=False)
    plt.pause(1e-3)

    best = inds[np.where(scores == min(scores))[0][0]]

    if indres == 1:
        return data_s[best, 0]
    else:
        return fit_halfgeneral(data_s, is_sorted=True, startind=best - indres, stopind=best + indres)


def fit_halfgen1(data, issorted=False, startind=None):
    data_s = np.sort(data) if not issorted else data
    startind = startind if startind is not None else len(data)

    fit = []
    for i in list(range(startind))[:5:-1]:
        data_sc = data_s[0:i]
        fit.append(kstest(data_sc, 'halfnorm', args=halfnorm.fit(data_sc))[0] * len(data_sc))

    x = 1


def bootstrap(
        system,
        # memory_estimate,
        batchsize=None,
        slopesmooth=3,
        slopethresh=0.05,
        slopethresh_ct=3,
        logger=None,
        plot=False,
        debug=False,
        **kwargs
):

    # nstate_cols = system.input_dim*(memory_estimate+1) + system.output_dim*memory_estimate
    kde_params = {name: val for name, val in locals().items() if name.startswith('kde_')}
    kld = []
    kld_prob1 = None

    xanity.log('starting bootstrapping')
    data_ids = []
    iteration = 0
    threshct = 0
    plotwindow = None
    database = None

    if not (system.database is not None and system.database.data is not None) or debug:
        # do a session of episodes:
        inputs = system.gen_random_inputs(batchsize, length=system.time_vector.size, color='mixed', amplitudes='log')

        xanity.log('running a batch')
        batch_data = system.sim_batch(inputs, system.time_vector)

        system.append_data(batch_data)

        if debug:
            kde_model = Kde_model(np.random.normal(size=(10, 2)), kde_params=kde_params)
            kld = [1, 2, 3]
            return kde_model, kld

    while True:
        iteration += 1

        # subject = ((np.moveaxis(system.database.cm, 1, 2) - system.database.cm_center)/system.database.cm_scale)
        subject = system.database.cm.standardized
        subject = subject.reshape(-1, 4)

        xanity.log('sampling from KDE model of cm dist')
        scan = np.random.uniform(
            low=(subject.min(axis=0) - 0.1 * system.database.cm_scale).reshape(-1),
            high=(subject.max(axis=0) + 0.1 * system.database.cm_scale).reshape(-1),
            size=(1000, subject.shape[1]),
        )

        xanity.log('building distribution model and sampling it')
        if kld_prob1 is None:

            kde_model = Kde_model(subject, kde_params=kde_params)
            kld_prob1 = np.exp(kde_model.score_samples(scan))
            continue

        else:
            kld_prob2 = kld_prob1
            kde_model = Kde_model(subject, kde_params=kde_params)
            kld_prob1 = np.exp(kde_model.score_samples(scan))

            kld.append(float(np.mean([
                stats.entropy(kld_prob1, kld_prob2),
                stats.entropy(kld_prob2, kld_prob1)])))

        if plot:
            dprob1 = kld_prob1
            dprob2 = kld_prob2
            if not plotwindow:
                plotwindow = BootstrapPlots()
            plotwindow.do_plots(dprob1=dprob1, dprob2=dprob2, error_data=subject, kld=kld)

        # check termination criteria
        if len(kld) >= slopesmooth:
            # fit to gamma or log-normal
            learnslope = np.max(kld[-slopesmooth:] / np.cumsum(kld)[-slopesmooth:])

            if learnslope < slopethresh or np.isinf(learnslope):
                threshct += 1
            else:
                threshct = 0

            xanity.log('iter: {:d} K-L_dist: {:4f} learnslope: {:4f} bs_threshct: {:d}'.format(
                iteration,
                kld[-1],
                learnslope,
                threshct))
        elif len(kld) >= 1:
            xanity.log('iter: {:d} K-L_dist: {:4f}'.format(
                iteration,
                kld[-1]))

        if threshct == slopethresh_ct:
            xanity.log('stopping bootstrapping')
            break

        # do a session of episodes:
        inputs = system.gen_random_inputs(batchsize, length=system.time_vector.size, color='mixed', amplitudes='log')

        xanity.log('running a batch')
        batch_data = system.sim_batch(inputs, system.time_vector)

        system.append_data(data=batch_data)

    return kde_model, kld


class Kde_model:
    def __init__(self, data, **kwargs):

        self.centers = data.mean(axis=0)
        self.scales = data.std(axis=0)

        if hasattr(data, 'compute'):
            self.centers = self.centers.compute()
            self.scales = self.scales.compute()

        self.model = KernelDensity(**kwargs).fit((data-self.centers)/self.scales)
        # **{name.split('kde_')[1]: val for name, val in kde_params.items()})

    def score_samples(self, points, *args, **kwargs):
        points = (points - self.centers)/self.scales
        return self.model.score_samples(points, *args, **kwargs)

    def sample(self, number, *args, **kwargs):
        samples = self.model.sample(number, *args, **kwargs)
        return samples * self.scales + self.centers


class LOSM_ROI:

    def __init__(self):
        self.id =np.random.randint(low=10000000, high=100000000 - 1)
        self.figure = None

    @staticmethod
    def bis(p, x_data):
        x0, x1, x2, y0, y1, y2 = p
        th_1 = np.arctan((y1-y0) / (x1-x0))
        th_2 = np.arctan((y2 - y1) / (y2 - y1))
        th_3 = th_2 - th_1
        th_4 = np.pi - th_3
        th_5 = th_4 / 2
        th_line = th_5 - th_1
        m = -np.tan(th_line)
        yy0 = -x1 * m + y1
        return x_data * m + yy0

    @staticmethod
    def pwl(x, p):
        p = np.array(p)
        x0 = np.split(p, 2)[0]
        y0 = np.split(p, 2)[1]
        assert x0.size == y0.size
        x0 = np.concatenate(([0.0], x0, [1.00]))
        y0 = np.concatenate(([0.0], y0, [1.00]))
        si = np.argsort(x0)
        x0 = x0[si]
        y0 = y0[si]
        condlist = []
        funclist = []
        for i in range(1, x0.size):
            myslope = (y0[i]-y0[i-1])/(x0[i]-x0[i-1])
            condlist.append(np.logical_and(x >= x0[i-1], x <= x0[i]))
            funclist.append(
                lambda x, slope=myslope, x0=x0[i-1], y0=y0[i-1]: slope*(x-x0) + y0
            )
        return np.piecewise(x, condlist, funclist)

    @staticmethod
    def curve_fit(x, y, pthresh):
        nsegs = 2
        x0 = np.linspace(0, 1, nsegs+1)[1:-1]
        y0 = y[np.round(x0*(x.shape[0]-1)).astype('int')]
        p0 = np.hstack((x0, y0))
        fun = lambda x,*p: LOSM_ROI.pwl(x, p)
        p, e = SPO.curve_fit(fun, x, y, p0=p0, bounds=(np.zeros(p0.shape), np.ones(p0.shape)))
        errorP = np.sum((y - fun(x, *p))**2)
        explainedP = np.sum(fun(x, *p)**2)
        while errorP/explainedP > pthresh:
            nsegs += 1
            x0 = np.linspace(0, 1, nsegs + 1)[1:-1]
            y0 = y[np.round(x0 * (x.shape[0] - 1)).astype('int')]
            p0 = np.hstack((x0, y0))
            p, e = SPO.curve_fit(fun, x, y, p0=p0, bounds=(np.zeros(p0.shape), np.ones(p0.shape)))
            errorP = np.sum((y - fun(x, *p)) ** 2)
            explainedP = np.sum(fun(x, *p) ** 2)

        return fun(x, *p), p

    @staticmethod
    def get_thresh(x, y, breaks):
        x_p = np.split(breaks, 2)[0]
        y_p = np.split(breaks, 2)[1]
        if x_p[0] != 0.0:
            x_p = np.concatenate(([0], x_p))
            y_p = np.concatenate(([0], y_p))
        if x_p[-1] != 1.0:
            x_p = np.concatenate((x_p, [1.0]))
            y_p = np.concatenate((y_p, [1.0]))

        bis = LOSM_ROI.bis(np.concatenate((x_p[-3:], y_p[-3:])), x)
        th_ind = np.where((y - bis)**2 == ((y - bis)**2).min())

        # block = []
        # for i in range(1, x_p.size):
        #     block.append(
        #         np.vstack((
        #             x[np.logical_and(x >= x_p[i - 1], x <= x_p[i])],
        #             fit[np.logical_and(x >= x_p[i - 1], x <= x_p[i])]
        #         )).T
        #     )
        #
        # for pt in range(x.size-1, 0, -1):
        #     d_t = Dist.cdist(np.array([(x[pt], y[pt])]), block[-1]).squeeze().min()
        #     d_pu = Dist.cdist(np.array([(x[pt], y[pt])]), block[-2]).squeeze().min()
        #     if d_pu<d_t:
        #         th_ind = pt
        #         break

        return th_ind, x[th_ind], y[th_ind]

    def fit(self, data, pthresh, plot=False):

        data1, _, _ = normalize(abs(data.squeeze()), min=0, max=1)
        data1_s = np.sort(data1)
        x_data = np.linspace(0, 1, data1.shape[0])

        fit, breaks = self.curve_fit(x_data, data1_s, pthresh=pthresh)
        thresh_ind, x, y = self.get_thresh(x_data, data1_s, breaks)

        if plot:
            self.figure = plt.figure(num=self.id)
            ax = self.figure.add_subplot(1, 1, 1)
            plt.plot(x_data, data1_s)
            plt.plot(x_data, fit)
            plt.scatter(x, y)
            ax.set_aspect('equal')

        return data1 >= y


class GMM_ROI:
    def __init__(self):
        self.id = np.random.randint(low=10000000, high=100000000 - 1)
        self.figure = None

    def fit(self, data, gmm_params, plot=False, one_sided=False, ncomponents=2):
        if one_sided:
            data, _, _ = normalize(abs(data), min=0)
        else:
            data, _, _ = normalize(data)

        data = np.sort(data).squeeze()

        scan = np.linspace(
            data[0] - 0.05 * (data[-1] - data[0]),
            data[-1] + 0.05 * (data[-1] - data[0]),
            1000,
        )

        model = skmix.GaussianMixture(n_components=ncomponents,
                                      **{name.split('gmm_')[1]: val for name, val in gmm_params.items()}
                                      ).fit(data.reshape((-1, 1)))

        mixp = np.exp(model.score(data.reshape((-1, 1))))

        scores = np.array([model.weights_[i] * stats.norm.pdf(
            data,
            loc=model.means_[i],
            scale=np.sqrt(model.covariances_[i])).reshape((-1,))
                           for i in range(len(model.weights_))])

        worst_point = np.where(abs(data) == abs(data).max())[0][0]
        tail_component = scores[:, worst_point] == scores[:, worst_point].max()

        interesting_data = (scores[tail_component, :].squeeze() == scores.max(axis=0)).reshape(-1)

        if plot:
            pdfs = np.array([model.weights_[i] * stats.norm.pdf(
                scan,
                loc=model.means_[i],
                scale=np.sqrt(model.covariances_[i])).reshape((-1,))
                             for i in range(len(model.weights_))])
            interesting_inds_plot = np.array(np.where(pdfs[tail_component, :] == pdfs.max(axis=0))[1])
            gap_inds_plot = np.where(np.diff(interesting_inds_plot) > 1)[0]
            interesting_inds_plot = np.split(interesting_inds_plot, gap_inds_plot + 1)
            rois = np.array([scan[seg[[0, -1]]] for seg in interesting_inds_plot])
            if self.figure is None:
                self.figure = ErrorSpacePlots(fignum=self.id)
            self.figure.plot(error_data=data, th_val=None, rois=rois, model=model, pdfs=pdfs, scan=scan)

        return interesting_data, mixp


def uniform_samples_in_pca_space(transformer, tail_data, n_samples, distribution='uniform', scale=1):
    if transformer == 'identity':
        data_t = tail_data
    else:

        x_weights = np.power(abs(get_error(tail_data)), 2).reshape((-1, 1))
        x_weights = x_weights / x_weights.max()
        center_point = np.where(x_weights == x_weights.max())[0][0]

        data_w = tail_data * x_weights.reshape(-1, 1)

        iloc = np.mean(data_w, axis=0)
        istd = np.std(data_w, axis=0)

        data_w = (data_w - iloc) / istd
        transformer.fit(data_w)
        data_t = transformer.transform(tail_data)

    if center_point is None:
        mean = np.zeros(data_t.shape[1])
    else:
        mean = data_t[center_point, :].reshape(-1)

    if distribution == 'normal' or distribution == 'gaussian':
        g_new = np.random.multivariate_normal(
            mean=mean,
            cov=scale * np.eye(data_t.shape[1]),
            size=n_samples
        )
    elif distribution == 'uniform':
        g_new = np.random.uniform(
            low=(mean - scale * abs(data_t).max(axis=0)).reshape(-1),
            high=(mean + scale * abs(data_t).max(axis=0)).reshape(-1),
            size=(n_samples, data_t.shape[1])
        )
    else:
        raise IOError

    if transformer == 'identity':
        return g_new, None
    else:
        return transformer.inverse_transform(g_new), transformer


def uniform_samples_in_vae_space(data, tail_inds, n_samples, distribution='uniform', scale=1):
    center_point = \
    np.where(abs(get_error(data.iloc[tail_inds])) == abs(get_error(data.iloc[tail_inds])).max(axis=0))[0][0]

    stimdata, stimloc, stimmag = normalize(get_stims(data).values, range=(0, 1))
    errdata, errloc, errmmag = normalize(abs(get_cv_response(data)[:, 0]), range=(0, 1))

    # # from http://blog.fastforwardlabs.com/2016/08/22/under-the-hood-of-the-variational-autoencoder-in.html
    (encoder, decoder), (latent_data, reconstructed_data) = vae.create_and_train(stimdata[tail_inds],
                                                                                 errdata[tail_inds])

    transformer.fit(data_w)
    data_t = transformer.transform(tail_data)

    if center_point is None:
        mean = np.zeros(data_t.shape[1])
    else:
        mean = data_t[center_point, :].reshape(-1)

    if distribution == 'normal' or distribution == 'gaussian':
        g_new = np.random.multivariate_normal(
            mean=mean,
            cov=scale * np.eye(data_t.shape[1]),
            size=n_samples
        )
    elif distribution == 'uniform':
        g_new = np.random.uniform(
            low=(mean - scale * abs(data_t).max(axis=0)).reshape(-1),
            high=(mean + scale * abs(data_t).max(axis=0)).reshape(-1),
            size=(n_samples, data_t.shape[1])
        )
    else:
        raise IOError

    if transformer == 'identity':
        return g_new, None
    else:
        return transformer.inverse_transform(g_new), transformer


def normalize(data, range=None, min=None, max=None, unit_qty=None):
    if range is not None:
        min = np.min(range)
        max = np.max(range)

    if min is None and max is None:
        loc = np.mean(data, axis=0)
        if unit_qty is None:
            unit_qty = np.std(data, axis=0)

    elif min is not None and max is None:
        loc = np.min(data, axis=0)
        if unit_qty is None:
            unit_qty = np.std(data, axis=0)

    elif min is None and max is not None:
        loc = np.max(data, axis=0)
        if unit_qty is None:
            unit_qty = np.std(data, axis=0)

    elif min is not None and max is not None:
        loc = np.min(data, axis=0)
        unit_qty = np.ptp(data, axis=0)
        return (min + np.divide(data - loc, unit_qty, where=unit_qty != 0) * (max - min)).reshape(
            data.shape), loc, unit_qty

    return (np.divide(data - loc, unit_qty, where=unit_qty != 0)).reshape(data.shape), loc, unit_qty


def trunc_kriging(data, tail_inds, n_samples, scale, distribution, columnnames=None, stimnames=None, plot=False):
    n_neighbors = 3*2**(data.shape[1]-5)
    tail_map = np.where(tail_inds)[0]
    tail_map = tail_map[np.argsort(abs(get_error(data.iloc[tail_map], columnnames=columnnames)).ravel())[::-1][:n_samples]]

    n_taildata = tail_map.size

    #head_map = np.random.choice(np.where(~tail_inds)[0], n_taildata, replace=False)
    head_map = np.concatenate((
        np.where(~tail_inds)[0],
        np.arange(tail_inds.size, data.shape[0])
    ))

    x, x_loc, x_scale = normalize(get_stims(data.iloc[np.concatenate((tail_map, head_map))], columnnames=columnnames, stimnames=stimnames))
    y, y_loc, y_scale = normalize(get_error(data.iloc[np.concatenate((tail_map, head_map))], columnnames=columnnames))
    y = y.reshape((-1, 1))

    x_tail = lambda: x[:n_samples]
    x_head = lambda: x[n_samples:]
    y_tail = lambda: y[:n_samples]

    del data

    dmat = Dist.cdist(x_tail(), x)

    # SCC = BC.SpectralCoclustering().fit(dmat)
    # s_n_map = dmat[np.argsort(SCC.row_labels_)]
    # s_n_map = s_n_map[:, np.argsort(SCC.column_labels_)]

    nb = np.zeros((dmat.shape[0], n_neighbors+1), dtype=int)
    nb_var = np.zeros(y_tail().shape)
    nb_sz = np.zeros(y_tail().shape)
    nb_pd = np.zeros(y_tail().shape)
    x_next = np.zeros(x_tail().shape)

    for i, (tx, ty) in enumerate(zip(x_tail(), y_tail())):
        nb[i] = np.argsort(dmat[i])[:n_neighbors+1]

        nb_x = x[nb[i]]
        nb_y = y[nb[i]]

        tranner = PCA(n_components=2, whiten=True)
        nb_x_t = tranner.fit_transform(nb_x-tx)
        gpmodel = GPR(normalize_y=True).fit(X=nb_x_t, y=nb_y-ty)

        fun = lambda X: -gpmodel.predict(X.reshape((1,-1)), return_std=True)[1].reshape(-1)
        x_next[i, :] = tx + tranner.inverse_transform(
            SPO.minimize(
                x0=np.zeros(nb_x_t.shape[1]),
                fun=fun,
                bounds=tuple(zip(0.5*nb_x_t.min(axis=0), 0.5*nb_x_t.max(axis=0)))
            ).x.reshape((1, -1))
        )

    if plot:
        if x_head().shape[1] == 3:
            ax = Axes3D(fig=plt.figure(num=16753))
            ax.cla()
            ax.scatter(xs=x_head()[:, 0], ys=x_head()[:, 1], zs=x_head()[:, 2], color='black')
            ax.scatter(xs=x_tail()[:, 0], ys=x_tail()[:, 1], zs=x_tail()[:, 2], color='red')
            ax.scatter(xs=x_next[:, 0], ys=x_next[:, 1], zs=x_next[:, 2], color='green')
            ax.legend(['head', 'tail', 'resampled'])
        elif x_head().shape[1] < 3:
            ax = plt.figure(num=16753).add_subplot(111)
            ax.cla()
            ax.scatter(x=x_head()[:, 0], y=x_head()[:, 1], color='black')
            ax.scatter(x=x_tail()[:, 0], y=x_tail()[:, 1], color='red')
            ax.scatter(x=x_next[:, 0], y=x_next[:, 1], color='green')
            ax.legend(['head', 'tail', 'resampled'])
        plt.pause(1e-9)

    return x_next*x_scale+x_loc


def forces(tx, t_dmat, nb_thresh):
    forces = np.zeros(tx.shape)
    for i, th_x in enumerate(tx):


        nb_x = th_x - x[nb[i]]
        nb_y = y - y[nb[i]]

        linmod = LR(fit_intercept=False).fit(X=nb_x, y=nb_y)

        v_x = x[nb[i]] - th_x
        d_x = np.linalg.norm(v_x, axis=1).reshape((-1, 1))
        u_x = np.divide(v_x, d_x, where=(d_x != 0.))

        pull = np.divide(nb_pd, (rterm + d_x ** 2), where=(d_x != 0.))
        #pull = nb_pd
        mag = np.clip(pull, a_min=0, a_max=None)

        forces[i, :] = np.mean(mag * u_x, axis=0)
    return forces


def solve_forces(plot=False):
    iter = 0
    tx = x_tail
    eps = dmat.min() *1000
    delta_thresh = 0
    deltas = eps * forces(tx)

    while np.linalg.norm(deltas, axis=1).min() > delta_thresh:
        if plot:
            if 'trace' not in locals():
                inds = np.random.choice(list(range(tx.shape[0])), 7, replace=False)
                trace = []
                ffig = plt.figure()
                sp1 = ffig.add_subplot(1, 2, 1, projection='3d')
                plt.title('movement')
                sp2 = ffig.add_subplot(1, 2, 2)
                plt.title('update mag')
            trace.append(tx[inds])
            if len(trace) > 20:
                trace = trace[-20:]
            d = np.array(trace)
            sp1.scatter(d[:, :, 0].reshape(-1), d[:, :, 1].reshape(-1), d[:, :, 2].reshape(-1),
                        c=(
                                iter +
                                np.tile(np.array(range(d.shape[1])), (1, d.shape[0]))
                        ).reshape(-1)
                        )
            if iter > 1:
                sp2.cla()
                sp2.plot(np.linalg.norm(np.diff(d, axis=0), axis=2).squeeze())
            plt.pause(1e-3)
        tx = tx + deltas
        deltas = eps * forces(tx)
        iter += 1
    return tx

    kpt[i, :] = solve_forces(plot=True)
    mu_step = np.mean(np.linalg.norm(kpt, axis=1))
    print('E[ ||step|| ] = {}'.format(mu_step * eps))
    x_cand = x[n_taildata:] + kpt * eps

    if plot:
        ffig = plt.figure()
        ax = Axes3D(ffig)
        ax.scatter(x[n_taildata:, 0], x[n_taildata:, 1], x[n_taildata:, 2], color='black')
        ax.scatter(x_cand[:, 0], x_cand[:, 1], x_cand[:, 2], color='red')
        plt.title('update')
        plt.pause(1e-3)

    return x_cand


def state_uniformity(density_est, batch_size=100, kld_slopesmooth=3, kld_slopethresh=0.1, kld_slopethreshct=3):

    pdf = None
    kld = []
    samps = None
    threshct = 0
    iteration = 0

    while True:

        if samps is None:
            samps = np.split(density_est.sample(batch_size), 2, axis=1)[0]
        else:
            samps = np.concatenate([
                samps,
                np.split(density_est.sample(batch_size), 2, axis=1)[0]
            ])

        pds = Dist.squareform(Dist.pdist(samps/samps.std(axis=0), metric='cityblock'))
        pds = np.array([np.median(row[list(range(0,i))+list(range(i+1,pds.shape[0]))]) for i, row in enumerate(pds)])
        grid = np.linspace(pds.min() - 0.1 * pds.ptp(), pds.max() + 0.1 * pds.ptp(), 1000).reshape((-1, 1))
        model = Kde_model(pds.reshape((-1, 1)))

        if pdf is None:
            pdf = np.exp(model.score_samples(grid))

        else:
            pdf2 = pdf
            pdf = np.exp(model.score_samples(grid))
            kld.append(np.mean([
                stats.entropy(pdf, pdf2),
                stats.entropy(pdf2, pdf)])
            )

        if len(kld) >= kld_slopesmooth:
            # fit to gamma or log-normal
            learnslope = np.max(kld[-kld_slopesmooth:] / np.cumsum(kld)[-kld_slopesmooth:])

            if learnslope < kld_slopethresh or np.isinf(learnslope):
                threshct += 1
            else:
                threshct = 0

            xanity.log('iter: {:d} K-L_dist: {:4f} learnslope: {:4f} bs_threshct: {:d}'.format(
                iteration,
                kld[-1],
                learnslope,
                threshct))

            if threshct == kld_slopethreshct:
                xanity.log('stopping bootstrapping')
                break

        elif len(kld) >= 1:
            xanity.log('iter: {:d} K-L_dist: {:4f}'.format(
                iteration,
                kld[-1]))

    return pds


def get_uniform_cm_sample_inds(system, estimated_memory, sample_size):

    # build density model for observed states
    csubj = system.database.cm.standardized.to_states(estimated_memory)
    subj = csubj.flat_signals

    tree = cKDTree(subj, leafsize=100)

    maxes = np.max(subj, axis=0)
    mins = np.min(subj, axis=0)
    synthetic = np.random.uniform(low=mins, high=maxes, size=(sample_size, mins.size))

    return tree.query(synthetic, k=1)[1]

    # targs = system.database.error.flat_signals
    # gpr = GPR(copy_X_train=False, n_restarts_optimizer=10).fit(subj, targs)
    # gpr.fit()
