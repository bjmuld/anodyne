from setuptools import setup
import os
import re
import inspect

package_name = 'anodyne'
short_description = "anodyne is a library for machine learning in n-dimensional time series observational data"

# get the full path to this file :
thispath = os.path.abspath(os.path.realpath(os.path.split(inspect.stack()[0][1])[0]))

# read actual long description from /README.md
if os.path.exists(os.path.join(thispath, 'README.md')):
    long_description = open(os.path.join(thispath, 'README.md'), 'r').read()
else: long_description = short_description

# read xanity version from /src/xanity/__init__.py :
for line in open(os.path.join(thispath, 'src', package_name,'__init__.py'), 'r').readlines():
    scraped = re.search(r'\s*__version__\s*=\s*[\"\'](\S+)[\"\']', line)
    if scraped:
        version = scraped.group(1).strip('\'\"').strip()


setup(

    name=package_name,
    version=str(version),

    install_requires=['scipy', 'waverunner', 'pyspectre', 'h5py', 'scikit-learn'],
    setup_requires=[],
    tests_require=[],

    packages={package_name: os.path.join('src', package_name)},
    package_dir={'': 'src'},

    scripts=[
    ],

    package_data={
        package_name: ['circuits/*']
    },

    # metadata to display on PyPI
    author="Barry Muldrey",
    author_email="barry@muldrey.net",
    description=short_description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    license="GPLv3",
    url="http://gitlab.com/bjmuld/anodyne/",   # project home page, if any
    project_urls={
        "Bug Tracker": "https://gitlab.com/bjmuld/anodyne/issues",
        "Documentation": "https://gitlab.com/bjmuld/anodyne",
    },

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: MacOS',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Natural Language :: English',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Visualization',
        'Topic :: Utilities',
    ],
)
